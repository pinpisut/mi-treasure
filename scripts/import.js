const admin = require("firebase-admin");

const fs = require('fs');
const csv = require("fast-csv");
const _ = require('lodash');

const { calcProjectCapitalGain } = require('./utilFunc');
const { genHistory } = require('./utilFunc');

const knex = require('knex')({
  client: 'mssql',
  connection: {
    host : '203.151.59.23',
    user : 'anandaagentts',
    password : 'T@gentTS17',
    database : 'TenancyService_UAT'
  }
});

const formateData = (results, key) => {
  return _.reduce(results, (result, value) => {
    return { ...result, [_.get(value, key)]: value}
  }, {});
}

const runProcess = async () => {
  const results = await knex.select('ProjectID', 'ProjectCode', 'ProjectName', 'ProjectNameTH').from('Project');
  const projectData = formateData(results, 'ProjectID');

  const stockResults = await knex('dbo.Item').where({ Enable: 1, ItemStatusCode: 1 }).groupBy('ProjectCode').count('item.ItemCode as stock').select('ProjectCode');
  const stockData = formateData(stockResults, 'ProjectCode');
  
  const serviceAccount = require('./mi-treasure-firebase-adminsdk-ipk3u-a01ae807b0.json');
  
  const PROJECT_COLLECTION = 'projects';
  
  admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://mi-treasure.firebaseio.com"
  });
  
  const db = admin.firestore();
  
  // var stream = fs.createReadStream("./scripts/MI Treasure - Project.csv");
  var stream = fs.createReadStream("./scripts/MI Treasure - Export-data-july2018.csv");

  var csvStream = csv.fromStream(stream, { headers: true, ignoreEmpty: true })
    .transform(function(data){
      // console.log('sale', data['2018-sale']);
      // console.log(_.toNumber(data['2018-sale'].replace(/,/g, '')));
      // console.log('projectData', projectData);

      const gps = _.split(data['GPS'], ', ');
      const name = data['Project name'];
      const id = `${data['Station']}.${_.kebabCase(name)}`;
      const projectId = _.trim(data['ProjectID']);

      const projectCode = _.get(projectData, `${projectId}.ProjectCode`, '');
      const projectTitleEN = _.get(projectData, `${projectId}.ProjectName`, '');
      const projectTitleTH = _.get(projectData, `${projectId}.ProjectNameTH`, '');
      const projectZoneID = _.get(projectData, `${projectId}.ZoneID`, '');

      const rentPrice = _.toNumber(data['2018-rent'].replace(/,/g, ''));
      const salePrice = _.toNumber(data['2018-sale'].replace(/,/g, ''));
      const rentalYield = Math.round(10000 * (12 * rentPrice) / salePrice) / 10000;
      const history = genHistory(data);
      const capitalGain = calcProjectCapitalGain(history, salePrice);
      // console.log('LOG >>>', history, capitalGain);
      const stock = _.get(stockData, [projectCode, 'stock'], 0);

      return {
        id,
        title: {
          en: name,
          th: projectTitleTH,
        },
        station: data['Station'],
        yearOpen: data['Year Open'],
        yearComplete: data['Year completed'],
        startingPersqm: _.toNumber(data['Starting price/ sq.m.'].replace(/,/g, '')),
        startingPrice: _.toNumber(data['Starting price'].replace(/,/g, '')),
        stock,
        current: {
          time: '2018',
          salePrice,
          rentPrice,
          updatedAt: admin.firestore.FieldValue.serverTimestamp(),
          rentalYield,
          capitalGain,
        },
        location: {
          position: {
            _lat: _.toNumber(_.trim(gps[0]).replace(/,/g, '')),
            _long: _.toNumber(_.trim(gps[1]).replace(/,/g, '')),
          }
        },
        history,
        integrations: {
          TheAgent: {
            ZoneID: projectZoneID,
            ProjectCode: projectCode,
            ProjectID: projectId,
            ProjectLink: (projectId != '' && projectId !== 'N/A') ? `https://www.theagent.co.th/project/d/${projectId}/${_.kebabCase(projectTitleEN)}` : '',

          }
        }
      }
    })
    .on("data", function(data){
      // console.log(data);
      
      const history = _.get(data, 'history');
      const info = _.omit(data, ['history']);
      // const info = data;
      console.log('info', info);
      db.collection(PROJECT_COLLECTION)
        .doc(info.id)
        .set(info)
        .then(() => {
          console.log("Document written with ID: ", info.id);
  
          // _.forEach(history, record => {
          // db.collection(PROJECT_COLLECTION).doc(info.id).collection('history')
          //     .doc(record.time)
          //     .set(record)
          //     .then(() => {
          //       console.log('wrote history', info.id, record.time);
          //     });
          // });
          
        })
        .catch(function(error) {
            console.error("Error adding document: ", error);
        });
    
      // console.log(json);
      
    })
    .on("end", function(){
      console.log("done");
    });
};

runProcess();
