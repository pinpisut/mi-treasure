
const moment = require('moment');
const _ = require('lodash');

const getSalePriceByYear = (year, history) => {
  const record = _.find(history, record => record.time === year);
  return record.salePrice;
}
const getCapitalGainFromTo = (from, to) => {
  return (from - to) / to;
}

const calcProjectCapitalGain = (history, priceToday) => {
  // const { history, salePrice: priceToday } = project;
  // const { salePrice: priceToday } = current;
  // console.log('history', history);
  // console.log('priceToday', priceToday);
  
  const today = moment();
  const last1Y = today.clone().subtract(1, 'years').format('YYYY');
  const last3Y = today.clone().subtract(3, 'years').format('YYYY');
  const last5Y = today.clone().subtract(5, 'years').format('YYYY');
  // console.log('last1Y', last1Y);
  // console.log('last3Y', last3Y);
  // console.log('last5Y', last5Y);

  const priceLast1Y = getSalePriceByYear(last1Y, history);
  const priceLast3Y = getSalePriceByYear(last3Y, history);
  const priceLast5Y = getSalePriceByYear(last5Y, history);
  // console.log('priceLast1Y', priceLast1Y);
  // console.log('priceLast3Y', priceLast3Y);
  // console.log('priceLast5Y', priceLast5Y);

  const cap1Y = getCapitalGainFromTo(priceToday, priceLast1Y);
  const cap3Y = getCapitalGainFromTo(priceToday, priceLast3Y);
  const cap5Y = getCapitalGainFromTo(priceToday, priceLast5Y);

  // console.log('cap1Y', cap1Y);
  // console.log('cap3Y', cap3Y);
  // console.log('cap5Y', cap5Y);

  const realValues = _.compact([cap1Y, cap3Y/3, cap5Y/5]);
  const capitalGain = _.sum(realValues) / _.size(realValues);
  // console.log('capitalGain', capitalGain);
  return Math.round(10000 * capitalGain) / 10000;
}

const genHistory = (data) => {
  return [
    {
      time: '2018',
      salePrice: _.toNumber(data['2018-sale'].replace(/,/g, '')),
      rentPrice: _.toNumber(data['2018-rent'].replace(/,/g, '')),
      sources: [
        {
          salePrice: _.toNumber(data['2018-sale'].replace(/,/g, '')),
          rentPrice: _.toNumber(data['2018-rent'].replace(/,/g, '')),
          source: 'hipflat',
        }
      ]
    },
    {
      time: '2017',
      salePrice: _.toNumber(data['2017-sale'].replace(/,/g, '')),
      rentPrice: _.toNumber(data['2017-rent'].replace(/,/g, '')),
      sources: [
        {
          salePrice: _.toNumber(data['2017-sale'].replace(/,/g, '')),
          rentPrice: _.toNumber(data['2017-rent'].replace(/,/g, '')),
          source: 'hipflat',
        }
      ]
    },
    {
      time: '2015',
      salePrice: _.toNumber(data['2015-sale'].replace(/,/g, '')),
      rentPrice: _.toNumber(data['2015-rent'].replace(/,/g, '')),
      sources: [
        {
          salePrice: _.toNumber(data['2015-sale'].replace(/,/g, '')),
          rentPrice: _.toNumber(data['2015-rent'].replace(/,/g, '')),
          source: 'hipflat',
        }
      ]
    },
    {
      time: '2013',
      salePrice: _.toNumber(data['2013-sale'].replace(/,/g, '')),
      rentPrice: _.toNumber(data['2013-rent'].replace(/,/g, '')),
      sources: [
        {
          salePrice: _.toNumber(data['2013-sale'].replace(/,/g, '')),
          rentPrice: _.toNumber(data['2013-rent'].replace(/,/g, '')),
          source: 'hipflat',
        }
      ]
    }
  ]
}

module.exports = {
  calcProjectCapitalGain,
  genHistory
}