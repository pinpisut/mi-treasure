const compare = function(projects) {
  return `
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h1>Compare Projects ${projects.map(p => p.title)}</h1>
      </div>
    </div>
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-body">
            <h5 class="card-title">Capital Gain & Retail Yield</h5>
            <canvas id="chart1" width="1200" height="600"></canvas>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-body">
            <h5 class="card-title">Average price / sq.m.</h5>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-body">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>Project Name</th>
                  ${projects.map(project => {
                    return `
                      <th>${project.title}</th>  
                    `;
                  })}
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Year Complete</td>
                  ${projects.map(project => {
                    return `
                      <td>${project.yearComplete}</td>  
                    `;
                  })}
                </tr>
                <tr>
                  <td>Average price/sq.m.</td>
                  ${projects.map(project => {
                    return `
                      <td>${project.salePrice}</td>  
                    `;
                  })}
                </tr>
                <tr>
                  <td>Average rent/sq.m.</td>
                  ${projects.map(project => {
                    return `
                      <td>${project.rentPrice}</td>  
                    `;
                  })}
                </tr>
                <tr>
                  <td>Starting Price</td>
                  ${projects.map(project => {
                    return `
                      <td>${project.startingPrice}</td>  
                    `;
                  })}
                </tr>
                <tr>
                  <td>Capital Gain</td>
                  ${projects.map(project => {
                    return `
                      <td>${project.capitalGain}</td>  
                    `;
                  })}
                </tr>
                <tr>
                  <td>Rental Yield</td>
                  ${projects.map(project => {
                    return `
                      <td>${project.rentalYield}</td>  
                    `;
                  })}
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.min.js"></script>
    <script type="text/javascript">
      var ctx = document.getElementById("chart1");
      var myChart = new Chart(ctx, {
          type: 'bar',
          data: {
            labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
            datasets: [{
              label: '# of Votes',
              data: [12, 19, 3, 5, 2, 3],
              backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
              ],
              borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
              ],
              borderWidth: 1
            }]
          },
          options: {
            animation: {
              duration: 0,
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
          }
      });
    </script>
  </div>
  `;
}

module.exports = compare;