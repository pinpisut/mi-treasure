const _ = require('lodash');
const express = require('express');
const cors = require('cors')({ origin: true });

const admin = require('../../config');
const statUtil = require('../../utils');

const app = express();
app.use(cors);

const template = require('./template');
const compare = require('./page-compare');


app.get('/compare', (req, res) => {
  const hours = (new Date().getHours() % 12) + 1 // london is UTC + 1hr;
  const initialState = {};

  const projectIds = _.split(req.query.ids, ',');
  
  const projectPromises = _.map(projectIds, id => {
    return admin
      .firestore()
      .collection('projects')
      .doc(id)
      .get()
      .then(doc => {
        const data = doc.data();
        console.log('data', data);
        return Object.assign({}, data, statUtil(data), {
          id: data.id,
        });
      })
  });

  return Promise.all(projectPromises)
    .then(projects => {
      console.log('projects', projects);
      
      const templatedHtml = template({
        title: 'Compare Projects',
        body: compare(projects),
        initialState: JSON.stringify(initialState)}
      );
    
      res.status(200).send(templatedHtml);
    });
});

module.exports = app;