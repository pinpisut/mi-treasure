const template = function(opts) {
  return `
  <!DOCTYLE html>
  <html>
    <head>
      <title>${opts.title}</title>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous" />
      <style type="text/css">
      body {
        background: #fafafa;
      }
      </style>
    </head>
    <body>
      <div id="root">${opts.body}</div>
    </body>
    <script>
      window.__initialState = ${opts.initialState}
    </script>
  </html>
  `;
}

module.exports = template;