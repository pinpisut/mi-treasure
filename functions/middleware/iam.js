const admin = require('../config');
const _ = require('lodash');

// const getUserPermission = (user) => {
//   const rolePromises = _.map(user.roles, role => getPermissionFromRole(role));
//   return Promise.all(rolePromises)
//     .then((result) => {
//       return _.uniq(_.flatten(result));
//     });
// }

// const getPermissionFromRole = (roleId, withPermissions = []) => {
//   return admin.firestore().collection('roles').doc(roleId).get()
//     .then(role => {
//       return role.data()
//     })
//     .then(role => {
//       if (role.extends) {
//         return getPermissionFromRole(role.extends, role.permissions);
//       } else {
//         return _.uniq([...role.permissions, ...withPermissions]);
//       }
//     });
//   }

const userHasPermission = (userPermissions, per) => _.includes(userPermissions, per);

const iam = (requiredRole) => (req, res, next) => {
  const { authorization } = req.headers;
  if (!authorization) res.status(401).json({ error: 'Unautorized' });

  const idToken = authorization.replace('Bearer ', '');
  // console.log('idToken', idToken);
  return admin.auth().verifyIdToken(idToken)
    .then(user => {
      const roles = _.get(user, 'roles', []);
      if (_.some(roles, role => role === requiredRole)) return next();
      return res.status(401).json({ error: 'not enough permission' });
    })
    .catch(error => {
      console.log('global error', error);
      res.status(500).json({ error })
    });
}

// .then(userPermissions => {
    //   if (_.every(requiredPermissions, per => userHasPermission(userPermissions, per))) return next();
    //   return res.status(401).json({ error: 'not enough permission' });
    // })

module.exports = iam;