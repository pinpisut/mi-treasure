const moment = require('moment');
const _ = require('lodash');

const getSalePriceByYear = (year, history) => {
  const record = _.find(history, record => record.time === year);
  return record.salePrice;
}
const getCapitalGainFromTo = (from, to) => {
  return (from - to) / to;
}

const calcProjectCapitalGain = project => {
  const { history, current } = project;
  const { salePrice: priceToday } = current;
  
  const today = moment();
  const last1Y = today.clone().subtract(1, 'years').format('YYYY');
  const last3Y = today.clone().subtract(3, 'years').format('YYYY');
  const last5Y = today.clone().subtract(5, 'years').format('YYYY');
  // console.log('last1Y', last1Y);
  // console.log('last3Y', last3Y);
  // console.log('last5Y', last5Y);

  const priceLast1Y = getSalePriceByYear(last1Y, history);
  const priceLast3Y = getSalePriceByYear(last3Y, history);
  const priceLast5Y = getSalePriceByYear(last5Y, history);
  // console.log('priceLast1Y', priceLast1Y);
  // console.log('priceLast3Y', priceLast3Y);
  // console.log('priceLast5Y', priceLast5Y);

  const cap1Y = getCapitalGainFromTo(priceToday, priceLast1Y);
  const cap3Y = getCapitalGainFromTo(priceToday, priceLast3Y);
  const cap5Y = getCapitalGainFromTo(priceToday, priceLast5Y);

  // console.log('cap1Y', cap1Y);
  // console.log('cap3Y', cap3Y);
  // console.log('cap5Y', cap5Y);

  const capitalGain = (cap1Y + (cap3Y / 3) + (cap5Y / 5)) / 3;
  return Math.round(100 * capitalGain) / 100;
}

const getProjectStat = project => {

  const { current, startingPrice, yearComplete } = project;
  const { salePrice, rentPrice } = current;

  const capitalGain = calcProjectCapitalGain(project);

  const rentalYield = Math.round(100 * (12 * rentPrice) / salePrice) / 100;

  return {
    yearComplete,
    salePrice,
    capitalGain,
    startingPrice,
    rentPrice,
    rentalYield,
  }
}

module.exports = getProjectStat