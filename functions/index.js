const _ = require('lodash');
const firebase = require('./config');
const functions = require('firebase-functions');
const api = require('./controllers/api');
const ssr = require('./controllers/ssr');
const admin = require('./controllers/admin');

exports.api = functions.https.onRequest(api); 
exports.ssr = functions.https.onRequest(ssr);
exports.admin = functions.https.onRequest(admin);

/*
exports.updateProjectHistory = functions.firestore
  .document('projects/{projectId}/history/{time}')
  .onWrite((change, context) => {
    // Get an object representing the document
    // e.g. {'name': 'Marie', 'age': 66}
    const projectId = context.params.projectId;
    const data = change.after.data();
    const prevData = change.before.data();
    console.log('isEqual', _.isEqual(data, prevData));
    if (_.isEqual(data, prevData)) return null;

    const sources = data.sources;

    const salePricesAllSources = _.compact(_.map(sources, source => source.salePrice)); 
    const rentPricesAllSources = _.compact(_.map(sources, source => source.rentPrice)); 
    const salePrice = _.sum(salePricesAllSources) / _.size(salePricesAllSources);
    const rentPrice = _.sum(rentPricesAllSources) / _.size(rentPricesAllSources);
    // access a particular field as you would any JS property
    // const name = newValue.name;

    console.log('written:', projectId, salePricesAllSources, salePrice);

    // perform desired operations ...
    const updateHistory = change.after.ref.set({
      salePrice,
      rentPrice,
      updatedAt: firebase.firestore.FieldValue.serverTimestamp(),
    }, { merge: true });

    const updateProjectStat = change.after.ref.parent.parent.set({
      current: {
        salePrice,
        rentPrice,
        time: '2018',
        updatedAt: firebase.firestore.FieldValue.serverTimestamp(),
      }
    }, { merge: true });

    return updateHistory; // .then(() => updateProjectStat);
  });
  */