const functions = require("firebase-functions");
const admin = require('firebase-admin');


if (process.env.GCP_PROJECT === 'mi-treasure') {
  console.log('init with MI Treasure');
  // develop
  const serviceAccount = require('./mi-treasure-certificate.json');
  admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: 'https://mi-treasure.firebaseio.com'
  });
} else {
  admin.initializeApp();
}

// production
// const serviceAccount = require('./mi-treasure-production-firebase-adminsdk-qkxaa-ec5c123f67.json');
// admin.initializeApp({
//   credential: admin.credential.cert(serviceAccount),
//   databaseURL: "https://mi-treasure-production.firebaseio.com"
// });

module.exports = admin;