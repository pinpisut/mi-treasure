import React from "react";
import _ from "lodash";
import numeral from "numeral";
import styled, { css } from "styled-components";
import { connect } from "react-redux";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import Modal from "@material-ui/core/Modal";
import IconClose from "@material-ui/icons/Close";

import StatItem from "../StatItem";

import { H3 } from "../Typography";

import { getProjectStat } from "../../utils/stat-utils";

const Container = styled.div`
  ${props =>
    props.layout === "B" &&
    `
    position: absolute;
    bottom: -80px;
    left: 0;
    right: 0;
    height: 80px;
    background: white;
  `};
`;

const StatRow = connect(state => ({
  layout: state.domain.design.layout,
  overviewPosition: state.domain.design.overviewPosition
}))(styled.div`
  display: flex;
  ${props =>
    props.layout === "A" &&
    css`
      flex-direction: column;

      @media (min-width: 768px) {
        flex-direction: row;
      }
    `}
  
  ${props =>
    props.layout === "B" &&
    css`
      flex-direction: column;

      @media (min-width: 768px) {
        flex-direction: column;
      }
    `}

  ${props =>
    props.overviewPosition === "B" &&
    css`
      flex-direction: row;
      @media (min-width: 768px) {
        flex-direction: row;
      }
    `}
`);

const SkyDronButton = styled.div`
  flex: 1;
  text-align: center;
  padding: 10px;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: row;
  background: #fff;
`;

const SkyDronButtonBlock = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  border: 2px solid #eb6323;
  padding: 10px 20px;
  flex-grow: 0;
  border: 2px solid #eb6323;
  cursor: pointer;
`;

const SkyDronButtonModal = styled.div`
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  position: absolute;
  width: 80vw;
  &:focus {
    outline: none;
  }
  > * {
    &:focus {
      outline: none;
    }
  }
`;

const CloseModal = styled.div`
  width: 30px;
  height: 30px;
  border: 2px solid #eb6323;
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: 50%;
  background: #eee;
  position: absolute;
  right: -15px;
  top: -15px;
  z-index: 3;
  cursor: pointer;
`;

const Label = styled.div``;

export default class extends React.PureComponent {
  static defaultProps = {
    projects: [],
    area: ""
  };

  state = {
    skydronModal: false
  };

  handleOpenSykDrone = () => {
    this.setState({ skydronModal: true });
  };

  handleCloseSykDrone = () => {
    console.log("handleCloseSykDrone");
    this.setState({ skydronModal: false });
  };

  render() {
    const { layout, pointOfInterest, projects } = this.props;
    console.log("Overview location", pointOfInterest);
    const locationType = _.get(pointOfInterest, "type");
    const locationId = _.get(pointOfInterest, "id", "");
    const locationSkydroneVideo = _.get(
      pointOfInterest,
      "skydrone_video",
      false
    );

    const allProjectStats = _.map(projects, getProjectStat);

    const capitalGainValues = _.compact(
      _.map(allProjectStats, stat => stat.capitalGain)
    );
    const rentPriceValues = _.compact(
      _.map(allProjectStats, stat => stat.rentPrice)
    );
    const rentalYieldValues = _.compact(
      _.map(allProjectStats, stat => stat.rentalYield)
    );
    const salePriceValues = _.compact(
      _.map(allProjectStats, stat => stat.salePrice)
    );
    const startingPriceValues = _.compact(
      _.map(allProjectStats, stat => stat.startingPrice)
    );

    const avg = {
      salePrice: _.sum(salePriceValues) / _.size(salePriceValues),
      capitalGain: _.sum(capitalGainValues) / _.size(capitalGainValues),
      startingPrice: _.sum(startingPriceValues) / _.size(startingPriceValues),
      rentPrice: _.sum(rentPriceValues) / _.size(rentPriceValues),
      rentalYield: _.sum(rentalYieldValues) / _.size(rentalYieldValues)
    };
    return (
      <Container layout={layout}>
        <div>
          <Card>
            <CardContent style={layout === "B" ? { padding: "4px 12px" } : {}}>
              {/* <Typography style={{ textAlign: 'center' }}>{_.get(pointOfInterest, 'title.en', `${_.get(pointOfInterest, 'position.location._lat')}, ${_.get(pointOfInterest, 'position.location._long')}`)}</Typography> */}
              <StatRow>
                <StatItem
                  label="Sale Price/Sq.m."
                  value={`฿ ${numeral(avg.salePrice).format("0,0a")}`}
                />
                <StatItem
                  label="Capital Gain"
                  value={numeral(avg.capitalGain).format("0.00%")}
                  suffix="%"
                />
                <StatItem
                  label="Starting Price"
                  value={`฿ ${numeral(avg.startingPrice).format("0.00a")}`}
                />
                <StatItem
                  label="Rent Price/Sq.m."
                  value={`฿ ${numeral(avg.rentPrice).format("0,0.00")}`}
                />
                <StatItem
                  label="Rental Yield"
                  value={numeral(avg.rentalYield).format("0.00%")}
                  suffix="%"
                />
                {locationType === "BTS" || locationType === "MRT" ? (
                  <SkyDronButton>
                    <SkyDronButtonBlock onClick={this.handleOpenSykDrone}>
                      <svg
                        width="18"
                        height="18"
                        viewBox="0 0 24 24"
                        style={{ cursor: "pointer" }}
                      >
                        <path
                          fill="#EB6323"
                          d="M18,14.5V11A1,1 0 0,0 17,10H16C18.24,8.39 18.76,5.27 17.15,3C15.54,0.78 12.42,0.26 10.17,1.87C9.5,2.35 8.96,3 8.6,3.73C6.25,2.28 3.17,3 1.72,5.37C0.28,7.72 1,10.8 3.36,12.25C3.57,12.37 3.78,12.5 4,12.58V21A1,1 0 0,0 5,22H17A1,1 0 0,0 18,21V17.5L22,21.5V10.5L18,14.5M13,4A2,2 0 0,1 15,6A2,2 0 0,1 13,8A2,2 0 0,1 11,6A2,2 0 0,1 13,4M6,6A2,2 0 0,1 8,8A2,2 0 0,1 6,10A2,2 0 0,1 4,8A2,2 0 0,1 6,6Z"
                        />
                      </svg>
                      <Label>
                        <H3
                          style={{
                            color: "#EB6323",
                            marginBottom: 0,
                            marginLeft: 8
                          }}
                        >
                          View Sky Drone
                        </H3>
                      </Label>
                    </SkyDronButtonBlock>
                  </SkyDronButton>
                ) : null}
              </StatRow>
            </CardContent>
          </Card>
        </div>
        <Modal
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
          open={this.state.skydronModal}
          onClose={this.handleCloseSykDrone}
        >
          <SkyDronButtonModal>
            <CloseModal onClick={this.handleCloseSykDrone}>
              <IconClose color="error" />
            </CloseModal>
            <video width="100%" controls src={locationSkydroneVideo} />
          </SkyDronButtonModal>
        </Modal>
      </Container>
    );
  }
}
