import React from "react";
import styled from "styled-components";
import _ from "lodash";
import { Radar, RadarChart, PolarGrid, PolarAngleAxis} from 'recharts';
import { connect } from 'react-redux';

import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import { withStyles } from '@material-ui/core/styles';

import { cleanFacilities } from '../../utils/facilities-utils';
import { getDistance } from '../../utils/map-utils';

const orange = '#f06027';

const Contanier = styled.div`
  .box-card {
    margin-bottom: 10px;
    box-shadow: 0 4px 6px 0 rgba(0, 0, 0, 0.1);
  }
  .box-card-content {
    display: flex;
    flex-direction: row;
    justify-content: flex-start;
  }
  .box-card-title{
    flex-basis: 40%;
    flex-shrink: 0;
  }
  .card-title {
    display: flex;
    align-items: center;
    font-size: 18px;
    font-weight: bold;
    line-height: 1.22;
    color: #303030;
    img {
      margin-right: 10px;
    }
  }
  .box-card-detail {
    display: flex;
    align-items: flex-start;
    flex-direction: column;
    justify-content: center;
  }
  .detail-name {
    color: #787878;
    padding: 0;
    font-size: 12px
  }
  .box-card-overall {
    margin-top: 22px;
    margin-bottom: 22px;
    box-shadow: 0 4px 6px 0 rgba(0, 0, 0, 0.1);
    position: relative;
    .overall {
      font-size: 24px;
      font-weight: bold;
      color: #ffffff;
      height: 68px;
      display: flex;
      .overall-title {
        border-top-left-radius: 4px;
        background-color: ${orange};
        flex-basis: 80%;
        display: flex;
        justify-content: flex-start;
        padding-left: 20px;
        align-items: center;
      }
      .overall-score {
        border-top-right-radius: 4px;
        background-color: ${orange};
        opacity: 0.7;
        flex-basis: 20%;
        display: flex;
        justify-content: center;
        align-items: center;
        text-align: center;
        flex-direction: column;
        line-height: 1;
        small {
          font-size: 18px;
          font-weight: bold;
        }
      }
    }
    .add-compare {
      position: absolute;
      bottom: 0;
      background-color: #f7f7f7;
      border-bottom-right-radius: 3px;
      border-bottom-left-radius: 3px;
      font-size: 14px;
      font-weight: 500;
      color: ${orange};
      padding: 10px 15px;
      width: 100%;
      display: flex;
      align-items: center;
    }
  }
  .line-chart{
    .box-place{
      display: flex;
      align-items: center;
      margin-bottom: 16px;
    }
    img{
        height: max-content;
        margin-right: 15px;
      }
    .box-place-title{
      width: 100%;
      .place-title{
      font-size: 16px;
      font-weight: 500;
      line-height: 1.57;
      color: #666666;
      }
    }
    .bar-progress{
      width: 100%;
      position: relative;
      .bar-bg{
        width: 100%;
        height: 7px;
        border-radius: 10px;
        background-color: #E7E7E7;
      }
      .bar-color{
        position: absolute;
        top: 0;
        left: 0;
        width: 50%;
        height: 7px;
        border-radius: 10px;
        background-color: #9e64eb;
      }
    }
  }
`;

const styles = {
  root: {
    background:'#e7e7e7',
    height: 7,
    borderRadius: '100px'
  },
  barColorPrimary: {
    backgroundColor: '#676398'
  }
};

const StyledTabs = withStyles({
  indicator: {
    top: 0,
  },
  textColorInherit: {
    color: '#121212',
  }
})(Tabs);

const StyledTab = withStyles({
  selected:{
    background: '#FAFAFA',
    color: '#f06027',
    fontWeight: 500,
  },
})(Tab);

// Find nearest in each group
const nearestFacilityByGroup = (groupedNearbyFacilities) => {
  return _.map(groupedNearbyFacilities, category => {
    const orderedData = _.orderBy(category, ['distance'], ['asc'])
    return orderedData[0];
  })
}

const calScore = (groupedNearbyFacilities, groupCateogries) => {
  const nearestFacilityInGroup = nearestFacilityByGroup(groupedNearbyFacilities);

  const sumScore = _.reduce(nearestFacilityInGroup, (sum, category) => {
    let score = 0;
    if(category.distance < 0.2) score = 100;
    else if(category.distance < 0.3 ) score = 80;
    else if(category.distance < 0.4) score = 60;
    else if(category.distance < 0.5) score = 40;
    return sum + score
    }, 0);

  const avgScore = sumScore/_.size(groupCateogries);
  return _.round(avgScore, 1);
}

const getDataScore = (groupedNearbyFacilities) => {
  const nearestFacilityInGroup = nearestFacilityByGroup(groupedNearbyFacilities);

  const score = _.map(nearestFacilityInGroup, (category) => {
    let score = 0;
    if(category.distance < 0.2) score = 100;
    else if(category.distance < 0.3 ) score = 80;
    else if(category.distance < 0.4) score = 60;
    else if(category.distance < 0.5) score = 40;
    return {...category, score}
    });

  return score;
}

const getDataChart = (nearestFacilityWithScore, groupCateogries) => {
  const defaultData = _.map(groupCateogries, category => ({ category, score: 0, fullMark: 100 }));
  const realData = _.reduce(nearestFacilityWithScore, (acc, facilityInGroup) => {
    return _.map(acc, accItem => {
      if (accItem.category === facilityInGroup.category) return { ...accItem, score: facilityInGroup.score };
      return accItem;
    })
  }, defaultData);

  return realData;
}

const mapStateToProps = (state, ownProps) => {
  // Clean Data
  const cleanedFacilities = cleanFacilities(state.facilities.data);
  const groupCateogries = cleanedFacilities.groupCateogries;

  // Find current lat, long position
  const currentLat = ownProps.pointOfInterest.location.position._lat;
  const currentLong = ownProps.pointOfInterest.location.position._long;

  // Find distance between facilities and current position
  const facilitiesWithDistance = _.map(state.facilities.data, facility => {
    return { ...facility, distance: getDistance(currentLat, currentLong, _.toNumber(facility.location.lat), _.toNumber(facility.location.lon)) };
  });

  // Find all facilities around the current point and group them
  const nearbyFacilities = _.filter(facilitiesWithDistance, facility => facility.distance < 0.5);
  const groupedNearbyFacilities = _.groupBy(nearbyFacilities, facility => facility.category);

  // Find overall score
  const overrallScore = calScore(groupedNearbyFacilities, groupCateogries);

  // Get data to render
  const nearestFacilityWithScore = getDataScore(groupedNearbyFacilities);
  const dataChart = getDataChart(nearestFacilityWithScore, groupCateogries);

  return {
    ...ownProps,
    groupCateogries,
    overrallScore,
    dataChart,
    groupedNearbyFacilities
  }
}

class Facilities extends React.PureComponent {

  state = {
    chart: 0,
  };

  handleChangeChart = (event, value) => {
    this.setState({ chart: value });
  };

  render() {
    const lineChartColor = {
      hospital: '#e47553',
      office: '#2ed0e6',
      shopping: '#ff9500',
      education: '#3e93d9',
      embassy: '#9e64eb',
      park: '#b5e62e'
    }

    const { chart } = this.state;
    const { overrallScore, groupCateogries, dataChart, getIconUrl, pointOfInterest, groupedNearbyFacilities } = this.props;
    const name = pointOfInterest.title.th;
    return (
      <Contanier>
        <Card className="box-card-overall">
          <div className="overall">
            <span className="overall-title">Overrall Score</span>
            <span className="overall-score">
              {overrallScore}
              <small>scores</small>
            </span>
          </div>
          <StyledTabs
            value={chart}
            onChange={this.handleChangeChart}
            indicatorColor="primary"
            textColor="primary"
          >
            <StyledTab label="Line Chart" />
            <StyledTab label="Spider Chart" />
          </StyledTabs>
          <CardContent style={{ minHeight: '100px', paddingBottom: '70px' }}>
            {chart === 0 &&
              <div className="line-chart">
              {_.map(dataChart, (item)=> {
                return (
                  <div className="box-place">
                    <img src={getIconUrl(item.category)} width={24} alt="facility-logo" />
                    <div className="box-place-title">
                      <div className="place-title">{_.upperFirst(item.category)}</div>
                      <div className="bar-progress">
                        <div className="bar-bg" />
                        <div className="bar-color"
                        style={{ backgroundColor: lineChartColor[item.category], width: `${item.score}%` }} />
                      </div>
                    </div>
                </div>
                )
              })}
              </div>
            }
            {chart === 1 &&
              <div className="spider-chart">
                <RadarChart width={435} height={360} data={dataChart}>
                  <PolarGrid />
                  <PolarAngleAxis dataKey="category" />
                  <Radar name="category" dataKey="score" stroke={orange} fill={orange} fillOpacity={0.6}/>
                </RadarChart>
              </div>
            }
          </CardContent>
          <div className="add-compare">Add {name} to Compare</div>
        </Card>
        <div>
          {_.map(groupCateogries, type => (
            <Card key={type} className="box-card">
              <CardContent>
                <div className="box-card-content">
                  <div className="box-card-title">
                    <div className="card-title">
                      <img src={getIconUrl(type)} width={24} alt="facility-logo" />{" "}
                      {_.upperFirst(type)}
                    </div>
                  </div>
                  <div className="box-card-detail">
                    {_.map(groupedNearbyFacilities, item => {
                      return _.map(item, place => {
                        if(place.category === type)
                          return (
                            <div className="detail-name" style={_.size(item) > 1 ? {'paddingBottom': '12px'} : {'paddingBottom': '0'}} key={place.id}>
                              {place.title.en}
                            </div>
                          )
                      })
                    })}
                  </div>
                </div>
              </CardContent>
            </Card>
            ))
          }
        </div>
      </Contanier>
    );
  }
}

export default connect(
  mapStateToProps,
)(withStyles(styles)(Facilities));

