import React from 'react';
import _ from 'lodash';

import { getDistance } from '../../utils/map-utils';


const getProjectWithinRadius = (projects, { lat, lng }, inner, outer) => {
  return _.filter(projects, p => {
    const pp = _.get(p, 'location.position');
    const d = getDistance(pp._lat, pp._long, lat, lng);
    return d <= 0.001 * outer && d >= 0.001 * inner;
  });
};

export default class extends React.PureComponent {
  render() {
    const { projects, location } = this.props;
    console.log('projects', projects);
    console.log('location', location);
    const lat = _.get(location, 'center._lat', 13.719610);
    const lng = _.get(location, 'center._long', 100.521563);

    const byDistance = {
      '0-200': getProjectWithinRadius(projects, { lat, lng }, 0, 200),
      '200-400': getProjectWithinRadius(projects, { lat, lng }, 200, 400),
      '400-600': getProjectWithinRadius(projects, { lat, lng }, 400, 600),
    };
    
    console.log('byDistance', byDistance);
    return (
      <div>
        {_.map()}
      </div>
    );
  }
}