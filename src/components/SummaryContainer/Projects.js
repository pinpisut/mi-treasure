import React from 'react';
import _ from 'lodash';
import styled, { css } from 'styled-components';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import Button from '@material-ui/core/Button';
import Popover from '@material-ui/core/Popover';
import Badge from '@material-ui/core/Badge';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';

import IconExpandMore from '@material-ui/icons/ExpandMore';
import IconVisibility from '@material-ui/icons/Visibility';
import IconVisibilityOff from '@material-ui/icons/VisibilityOff';
import IconFavorite from '@material-ui/icons/Favorite';
import IconFavoriteBorder from '@material-ui/icons/FavoriteBorder';
import IconCheckboxOutline from '@material-ui/icons/CheckBoxOutlineBlank';

import { addAllToCompare, removeAllFromCompare } from '../../actions/compare-actions';

import ProjectCardA from '../ProjectCard';
import ProjectCardB from '../ProjectCardB';


const Container = styled.div``;

const HeaderWrapper = styled.div`
  padding: 12px 12px 0;
  display: flex;
  justify-content: space-between;
`;

const ProjectsWrapper = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  padding-top: 16px;
  padding-bottom: 26px;
  padding-left: 12px;
  padding-right: 12px;

  > div {
    flex: 1 0 396px;
    ${props => props.layout === 'A' &&
      `margin: 0 8px;`
    }
    ${props => props.layout === 'B' &&
      `margin-bottom: 8px;`
    }
  }

  ${props => props.layout === 'B' &&
    css`
      flex-direction: column;
      > div {
        flex: 1 0 auto;
        width: 100%;
      }
    `
  }
`;

const mapStateToProps = (state, ownProps) => {
  // Domain
  const mapReducer = _.get(state, 'domain.map');
  const designProjectCard = _.get(state, 'domain.design.projectCard');
  const { visibleOnMapProjects } = mapReducer;

  return {
    ...ownProps,
    userSelectedProjects: visibleOnMapProjects,
    layout: state.domain.design.layout,
    compareProjectIds: state.domain.compare.ids,
    designProjectCard,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    clickAddAllToCompare: (ids) => dispatch(addAllToCompare(ids)),
    clickRemoveAllFromCompare: () => dispatch(removeAllFromCompare()),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(class extends React.PureComponent {
  static defaultProps = {
    projects: [],
  }

  state = {
    openMenu: false,
  }
  
  openMenu = (e) => {
    e.preventDefault();
    this.setState({ openMenu: true });
  }
  handleCloseMenu = (e) => {
    e.preventDefault();
    this.setState({ openMenu: false });
  }

  getDesignProjectCard = (type) => {
    switch (type) {
      case 'A': return ProjectCardA;
      case 'B': return ProjectCardB;
      default : return <div />;
    }
  }

  render() {
    const { layout, projects, pointOfInterest, userSelectedProjects, onSelectProject, poi, compareProjectIds } = this.props;
    // const projectIds = _.map(projects, p => p.id);
    // const { onToggleSelectAll, clickAddAllToCompare, clickRemoveAllFromCompare } = this.props;
    const { designProjectCard } = this.props;
    const ProjectCard = this.getDesignProjectCard(designProjectCard); 
    // const { openMenu } = this.state;
    return (
      <Container>
        <HeaderWrapper>
          {/* <Button
            variant="outlined"
            buttonRef={c => this.anchorEl = c}
            onClick={this.openMenu}
          >
            <IconCheckboxOutline />
            <IconExpandMore />
          </Button> */}
          {/* <Popover
            anchorEl={this.anchorEl}
            open={openMenu}
            onClose={this.handleCloseMenu}
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'left',
            }}
            transformOrigin={{
              vertical: 'top',
              horizontal: 'left',
            }}
          >
            <List>
              <ListItem button onClick={(e) => onToggleSelectAll(e, true)}>
                <ListItemIcon><IconVisibility /></ListItemIcon>
                <ListItemText>View All on Map</ListItemText>
              </ListItem>
              <ListItem button onClick={(e) => onToggleSelectAll(e, false)}>
                <ListItemIcon><IconVisibilityOff /></ListItemIcon>
                <ListItemText>Hide All from Map</ListItemText>
              </ListItem>
              <ListItem button onClick={() => clickAddAllToCompare(projectIds)}>
                <ListItemIcon><IconFavorite /></ListItemIcon>
                <ListItemText>Add All To Compare</ListItemText>
              </ListItem>
              <ListItem button onClick={clickRemoveAllFromCompare}>
                <ListItemIcon><IconFavoriteBorder /></ListItemIcon>
                <ListItemText>Remove All from Compare</ListItemText>
              </ListItem>
            </List>
          </Popover> */}
          {_.size(compareProjectIds) > 0 && 
            <Badge color="primary" badgeContent={_.size(compareProjectIds)}>
              <Button
                color="primary"
                component={Link}
                target="_blank"
                to={`/compare/${poi}/${_.join(compareProjectIds, ',')}`}
                style={{ display: 'block', width: '100%', background: 'white', fontSize: '1rem', boxShadow: '0 4px 6px 0 rgba(0, 0, 0, 0.05)' }}
              >
                <span className="icon-compare" /> Compare
              </Button>
            </Badge>
          }
        </HeaderWrapper>
        <ProjectsWrapper layout={layout}>
          {_.map(projects, (project, index) =>
            <ProjectCard
              key={project.id}
              {...project}
              reference={index + 1}
              pointOfInterest={pointOfInterest}
              onChange={onSelectProject}
              checked={_.includes(userSelectedProjects, project.id)}
            />
          )}
        </ProjectsWrapper>
      </Container>
    );
  }
});