import React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import _ from 'lodash';
import Divider from '@material-ui/core/Divider';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';

import Button from '../Button';
import DesignSetting from '../DesignSetting';

import Logo from '../Logo';

const Container = styled.div`
  height: 60px;
  background: white;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  -webkit-print-color-adjust: exact;
`;


const Nav = styled.div`
  display: none;
  @media (min-width: 768px) {
    display: flex;
  }
`;

const NavIcon = styled.span`
  margin-right: 2px;
  font-size: 1.3em;
  position: relative;
  top: -2px;
`;

class UserDropdown extends React.Component {

  static defaultProps = {
    user: {},
  }
  state = {
    open: false,
  }

  handleClose = () => {
    this.setState({ open: false });
  }

  render() {
    const { open } = this.state;
    const { user, auth } = this.props;
    return (
      <React.Fragment>
        <Button buttonRef={c => this.anchorEl = c} onClick={() => this.setState({ open: true })}>
          {user &&
            <div style={{ display: 'flex', alignItems: 'center', height: 47 }}>
              <img src={_.get(user, 'photoURL')} width={24} height={24} alt="User" style={{ marginRight: 5, marginTop: -4, borderRadius: 12, overflow: 'hidden' }} /> 
              <div>{_.get(user, 'displayName', 'User')}</div>
            </div>
          }
        </Button>
        <Menu
          open={open}
          anchorEl={this.anchorEl}
          onClose={this.handleClose}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'right',
          }}
          transformOrigin={{
            vertical: 'top',
            horizontal: 'right',
          }}
        >
          <MenuItem onClick={auth.signOut}>Design Setting</MenuItem>
          <DesignSetting />
          <Divider />
          {_.includes(_.get(user, 'roles'), 'mi-treasure.admin') && <MenuItem component={Link} to="/admin">Admin</MenuItem>}
          <MenuItem onClick={auth.signOut}>Logout</MenuItem>
        </Menu>
      </React.Fragment>
    );
  }
}

export default class extends React.Component {
  render() {
    const { user, auth } = this.props;
    return (
      <Container>
        <Logo />
        <Nav>
          <Button><NavIcon className="icon-menu-home" /> Home</Button>
          <Button><NavIcon className="icon-menu-smartmap" /> MI Smart Map</Button>
          <Button><NavIcon className="icon-menu-article" /> Articles</Button>
          <Button><NavIcon className="icon-menu-review" /> Reviews</Button>
        </Nav>
        <UserDropdown user={user} auth={auth} />
      </Container>
    );
  }
}