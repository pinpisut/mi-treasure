import styled from 'styled-components';
const watermark = require('./watermark-the-agent.png');

export default styled.div`
    -webkit-print-color-adjust: exact;
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background: url(${watermark}) left top repeat;
    background-size: 25%;
    z-index: 99999;
    opacity: 0.01;
    width: 100%;
    height: 100%;
    pointer-events: none;
`;