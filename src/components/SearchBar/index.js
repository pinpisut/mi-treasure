import React from "react";
import _ from "lodash";

import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import styled from "styled-components";
import Button from "@material-ui/core/Button";
import Hidden from "@material-ui/core/Hidden";
import IconMap from "@material-ui/icons/Map";
import IconPlace from "@material-ui/icons/Place";

import Input from "@material-ui/core/Input";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import ListItemText from "@material-ui/core/ListItemText";
import Select from "@material-ui/core/Select";
import Checkbox from "@material-ui/core/Checkbox";

import Highlighter from "react-highlight-words";

import * as Color from "../Color";
import { H6, P } from "../Typography";

import InputAreaSearch from "../InputAreaSearch";
// import ContentTab from '../../components/ContentTab';

const BREAKPOINT = 600;

const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  @media (min-width: ${BREAKPOINT}px) {
    flex-direction: row;
  }
`;

const InputAreaSearchWrapper = styled.div`
  flex-basis: 100%;
  position: relative;
  padding: 4px 0;
  box-sizing: border-box;
  @media (min-width: ${BREAKPOINT}px) {
    flex-basis: 30%;
    padding: 0 4px;
  }
  @media (max-width: 959px) {
    flex-basis: 45%;
    padding: 0 4px;
  }
`;

const ButtonToggleFilter = styled(Button).attrs({
  style: {
    position: "absolute",
    right: 5,
    top: 0,
    color: Color.primary,
    background: "#f7f7f7"
  }
})``;

const MoreFilterDesktop = styled.div`
  display: none;

  @media (min-width: ${BREAKPOINT}px) {
    display: flex;
    flex-direction: row;
    flex-basis: 100%;
    box-sizing: border-box;

    flex-basis: 55%;
    padding: 0 4px;
  }

  @media print {
    display: flex;
  }
`;

const MoreFilterMobile = styled.div`
  position: absolute;
  top: 100%;
  left: 0;
  right: 0;
  background: black;

  display: ${props => (props.open ? "block" : "none")};

  > div {
    padding: 7px 14px;
  }

  @media (min-width: ${BREAKPOINT}px) {
    display: none;
  }

  @media print {
    display: none;
  }
`;

const ActionWrapper = styled.div`
  display: flex;
  flex-direction: row;
  flex-basis: 100%;
  box-sizing: border-box;

  @media (min-width: ${BREAKPOINT}px) {
    flex-basis: 10%;
    padding: 0 4px;
  }
`;

const InputBudgetWrapper = styled.div`
  flex-basis: 50%;
  width: 50%;

  padding: 0;
  @media (min-width: ${BREAKPOINT}px) {
    padding: 0 4px;
  }
`;
const InputYearCompleteWrapper = styled.div`
  flex-basis: 50%;
  width: 50%;
  padding: 0;
  @media (min-width: ${BREAKPOINT}px) {
    padding: 0 4px;
  }
`;

const StyledSelect = styled.select`
  box-sizing: border-box;
  padding: 8px 12px;
  display: block;
  border-radius: 4px;
  width: 100%;
  background: white;
  border: 1px solid #d9d9d9;
  outline: none;
  font-size: 14px;
  height: 35px;
`;
const StyledMUSelect = styled(Select)`
  box-sizing: border-box;
  display: block;
  border-radius: 4px;
  width: 100%;
  background: white;
  border: 1px solid #d9d9d9;
  outline: none;
  font-size: 14px;
  height: 35px;
`;

const LabelInput = styled(P)`
  margin: 0 !important;
`;

const Option = styled.div`
  display: flex;
  align-items: center;
  padding: 4px;
`;

const IconWrapper = styled.div`
  margin-right: 20px;
`;
const dropdownStyle = {
  PaperProps: {
    style: {
      marginTop: 60
    }
  }
};
export default connect(state => ({
  compareProjectIds: state.domain.compare.ids
}))(
  withRouter(
    class SearchBar extends React.PureComponent {
      state = {
        fixedTop: false,
        showMobileFilter: false,
        isOpen: false,
        areas: [],
        locations: [],
        projects: [],
        area: "",
        weightby: "",
        type: "condo",
        budget: [],
        yearComplete: []
      };

      componentDidMount() {
        window.addEventListener("scroll", this.handleScroll);
      }

      componentWillUnmount() {
        window.removeEventListener("scroll", this.handleScroll);
      }

      /* Mobile Only */
      toggleMobileFilter = e => {
        this.setState({
          showMobileFilter: !this.state.showMobileFilter
        });
      };

      hideMobileFilter = e => {
        this.setState({
          showMobileFilter: false
        });
      };
      /* End of Mobile Only */

      handleScroll = e => {
        if (!this.state.fixedTop && window.scrollY >= 60) {
          this.setState({
            fixedTop: true
          });
        } else if (this.state.fixedTop && window.scrollY < 60) {
          this.setState({
            fixedTop: false
          });
        }
      };

      handleChangeLocation = selectedOption => {
        const { history } = this.props;
        this.setState({ selectedOption });
        history.push(`/map/${selectedOption.value}`);
      };

      handleChangeBudget = event => {
        this.setState({ budget: event.target.value });
        this.props.onFilterChange("budget", event.target.value);
      };

      handleChangeYearComplete = e => {
        this.setState({ yearComplete: e.target.value });
        this.props.onFilterChange("yearComplete", e.target.value);
      };

      handleClearFilter = () => {
        this.setState({ budget: [], yearComplete: [] });
        this.props.handleClearFilter();
      };

      renderSearchOption = option => {
        return (
          <Option key={option.value}>
            <IconWrapper>
              {option.type === "area" && <IconMap />}
              {option.type === "project" && <IconPlace />}
            </IconWrapper>
            <Highlighter
              highlightClassName="hl"
              searchWords={[this._inputValue]}
              autoEscape={true}
              textToHighlight={option.label || ""}
            />
          </Option>
        );
      };

      render() {
        const { pointOfInterest, query, yearRanges } = this.props;
        const { budget, yearComplete } = query;
        const { fixedTop, showMobileFilter } = this.state;
        return (
          <Container fixedTop={fixedTop}>
            <InputAreaSearchWrapper>
              <InputAreaSearch
                db={this.props.db}
                value={_.get(pointOfInterest, "id")}
                onChange={this.handleChangeLocation}
              />
              <Hidden smUp>
                <ButtonToggleFilter onClick={this.toggleMobileFilter}>
                  Filter
                </ButtonToggleFilter>
              </Hidden>
            </InputAreaSearchWrapper>
            <MoreFilterDesktop>
              <InputBudgetWrapper>
                <StyledMUSelect
                  name="budget"
                  disableUnderline={true}
                  multiple
                  value={this.state.budget}
                  onChange={this.handleChangeBudget}
                  input={<Input id="select-multiple-checkbox" />}
                  displayEmpty
                  MenuProps={dropdownStyle}
                  renderValue={selected => {
                    if (_.size(selected) > 0) {
                      const selectedLabelArr = _.map(
                        _.filter(budget.options, o => {
                          return _.includes(selected, o.value);
                        }),
                        bg => bg.label
                      );
                      return selectedLabelArr.join(", ");
                    } else {
                      return <LabelInput>ช่วงราคาทั้งหมด</LabelInput>;
                    }
                  }}
                >
                  {_.map(budget.options, opt => (
                    <MenuItem key={opt.value} value={opt.value}>
                      <Checkbox
                        checked={_.includes(this.state.budget, opt.value)}
                      />
                      <ListItemText primary={opt.label} />
                    </MenuItem>
                  ))}
                </StyledMUSelect>
              </InputBudgetWrapper>
              <InputYearCompleteWrapper>
                <StyledMUSelect
                  name="yearComplete"
                  disableUnderline={true}
                  multiple
                  value={this.state.yearComplete}
                  onChange={this.handleChangeYearComplete}
                  input={<Input id="select-multiple-checkbox-yearComplete" />}
                  displayEmpty
                  MenuProps={dropdownStyle}
                  renderValue={selected => {
                    if (_.size(selected) > 0) {
                      const selectedLabelArr = _.map(
                        _.filter(yearRanges, o => {
                          return _.includes(selected, `${o.min}-${o.max}`);
                        }),
                        bg => bg.label
                      );
                      return selectedLabelArr.join(", ");
                    } else {
                      return <LabelInput>ปีที่สร้างเสร็จ</LabelInput>;
                    }
                  }}
                >
                  {_.map(yearRanges, range => (
                    <MenuItem
                      key={`${range.min}-${range.max}`}
                      value={`${range.min}-${range.max}`}
                    >
                      <Checkbox
                        checked={_.includes(
                          this.state.yearComplete,
                          `${range.min}-${range.max}`
                        )}
                      />
                      <ListItemText primary={range.label} />
                    </MenuItem>
                  ))}
                </StyledMUSelect>
              </InputYearCompleteWrapper>
              <div style={{ marginLeft: 10 }}>
                <Button
                  variant="raised"
                  style={{ width: "100%", padding: "8px 12px", backgroundColor: 'transparent', border: '1px solid white' }}
                  onClick={this.handleClearFilter}
                >
                  <H6 style={{ margin: 0, color: "white" }}>Clear</H6>
                </Button>
              </div>
            </MoreFilterDesktop>

            <MoreFilterMobile open={showMobileFilter}>
              <div>
                <StyledMUSelect
                  name="budget"
                  disableUnderline={true}
                  multiple
                  value={this.state.budget}
                  onChange={this.handleChangeBudget}
                  input={<Input id="select-multiple-checkbox" />}
                  style={{ padding: 0, width: "100%" }}
                  displayEmpty
                  MenuProps={dropdownStyle}
                  placeholder="ช่วงราคาทั้งหมด"
                  renderValue={selected => {
                    if (_.size(selected) > 0) {
                      const selectedLabelArr = _.map(
                        _.filter(budget.options, o => {
                          return _.includes(selected, o.value);
                        }),
                        bg => bg.label
                      );
                      return selectedLabelArr.join(", ");
                    } else {
                      return "";
                    }
                  }}
                >
                  {_.map(budget.options, opt => (
                    <MenuItem key={opt.value} value={opt.value}>
                      <Checkbox
                        checked={_.includes(this.state.budget, opt.value)}
                      />
                      <ListItemText primary={opt.label} />
                    </MenuItem>
                  ))}
                </StyledMUSelect>
              </div>
              <div>
                <StyledMUSelect
                  name="yearComplete"
                  disableUnderline={true}
                  multiple
                  value={this.state.yearComplete}
                  onChange={this.handleChangeYearComplete}
                  input={<Input id="select-multiple-checkbox-yearComplete" />}
                  style={{ padding: 0, width: "100%" }}
                  displayEmpty
                  MenuProps={dropdownStyle}
                  renderValue={selected => {
                    if (_.size(selected) > 0) {
                      const selectedLabelArr = _.map(
                        _.filter(yearRanges, o => {
                          return _.includes(selected, `${o.min}-${o.max}`);
                        }),
                        bg => bg.label
                      );
                      return selectedLabelArr.join(", ");
                    } else {
                      return "ปีที่สร้างเสร็จ";
                    }
                  }}
                >
                  {_.map(yearRanges, range => (
                    <MenuItem
                      key={`${range.min}-${range.max}`}
                      value={`${range.min}-${range.max}`}
                    >
                      <Checkbox
                        checked={_.includes(
                          this.state.yearComplete,
                          `${range.min}-${range.max}`
                        )}
                      />
                      <ListItemText primary={range.label} />
                    </MenuItem>
                  ))}
                </StyledMUSelect>
              </div>
              <div>
                <Button
                  variant="raised"
                  style={{ width: "100%" }}
                  onClick={this.hideMobileFilter}
                >
                  Hide
                </Button>
              </div>
            </MoreFilterMobile>
            {/* <ActionWrapper>
          {_.size(compareProjectIds) > 0 && 
            <Badge color="primary" badgeContent={_.size(compareProjectIds)}>
              <Button
                color="primary"
                component={Link}
                target="_blank"
                to={`/compare/${poi}/${_.join(compareProjectIds, ',')}`}
                style={{ display: 'block', width: '100%' }}
              >
                <span className="icon-compare" /> Compare
              </Button>
            </Badge>
          }
        </ActionWrapper> */}
         {/* <ContentTab /> */}
          </Container>
        );
      }
    }
  )
);
