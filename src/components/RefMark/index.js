import React from 'react';
import styled from 'styled-components';
import { H6 } from '../Typography';


const Holder = styled.div`
  position: relative;
`;

const RefMarkOval = styled.div`
  width: 20px;
  height: 20px;
  background-color: #ffffff;
  border-radius: 50%;
  text-align: center;

  position: absolute;
  top: 15%;
  left: 20%;
`;

export default (props) => (
  <Holder>
    <svg xmlns="http://www.w3.org/2000/svg" width="33" height="39" viewBox="0 0 33 39">
      <path fill={props.color} fill-rule="evenodd" stroke={props.color} d="M15.797 37.305L5.54 27.163a15.204 15.204 0 0 1 0-21.674c6.053-5.985 15.867-5.985 21.92 0a15.204 15.204 0 0 1 0 21.674L17.203 37.305a1 1 0 0 1-1.406 0z"/>
    </svg>
    <RefMarkOval><H6>{props.reference}</H6></RefMarkOval>
  </Holder>
) 