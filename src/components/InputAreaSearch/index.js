import React from 'react';
import Downshift from 'downshift';
import styled from 'styled-components';
import _ from 'lodash';
import Fuse from 'fuse.js';
import { connect } from 'react-redux';

import * as Color from '../Color';

const IconBts = require('./icon-bts.png');
const IconMrt = require('./icon-mrt.png');
const IconProject = require('./icon-project.png');

const getTypeIcon = type => {
  switch (type) {
    case 'BTS': return  <img src={IconBts} width={20} alt="BTS" />;
    case 'MRT': return  <img src={IconMrt} width={20} alt="MRT" />;
    case 'PROJECT': return  <img src={IconProject} width={20} alt="PRJ" />;
    default: return null;
  }
}

const fuseOptions = {
  shouldSort: true,
  threshold: 0.2,
  location: 0,
  distance: 100,
  maxPatternLength: 32,
  minMatchCharLength: 1,
  keys: [
    "title.en",
    "title.th"
  ]
};

const filterItems = (items, inputValue) => {
  const fuse = new Fuse(items, fuseOptions);
  return _.sortBy(fuse.search(inputValue), item => item.type);
}


const Input = styled.input`
  box-sizing: border-box;
  padding: 8px 12px;
  display: block;
  border-radius: 4px;
  width: 100%;
  background: white;
  border: 1px solid #d9d9d9;
  outline: none;
  font-size: 14px;

  &:focus {
    border-color: ${Color.primary};
  }
`;

const Dropdown = styled.div`
  position: absolute;
  border: 1px solid #ccc;
  border-radius: 4px;
  background: white;
  width: 100%;
  left: 0;
`;

const DropdownItem = styled.a`
  display: flex;
  flex-direction: row-reverse;
  align-items: center;
  padding: 12px 24px;
  text-align: left;
  cursor: pointer;

  background: white;
  color: gray;
  &:hover {
    background: ${Color.primary};
    color: white;
  }
`;

const DropdownItemIconHolder = styled.div`
  flex-basis: 50px;
  opacity: 0.4;
  text-align: left;
`;

const DropdownItemLabelHolder = styled.div`
  flex-grow: 1;
`;

const mapStateToProps = (state) => {
  return {
    items: _.map(_.get(state, 'entities.pointOfInterests.entities'), poi => {
      return {
        ...poi,
        value: poi.id,
        label: `${_.get(poi, 'title.en', '')}`,
      }
    }),
  }
}

export default connect(
  mapStateToProps,
)(class InputAreaSearch extends React.PureComponent {

  renderSuggestions = (items, { getItemProps, inputValue, selectedItem, highlightedIndex }) => {
    return _.map(items, (item, index) => (
      <DropdownItem
        {...getItemProps({item})}
        key={item.id}
        style={{
          backgroundColor: highlightedIndex === index ? 'rgba(236, 112, 36, 0.2)' : 'white',
          color: highlightedIndex === index ? '#666666' : '#666666',
          fontWeight: selectedItem === item.value ? 'bold' : 'normal',
        }}
      >
        <DropdownItemLabelHolder>
          {item.label}
        </DropdownItemLabelHolder>
        <DropdownItemIconHolder>{getTypeIcon(item.type)}</DropdownItemIconHolder>
      </DropdownItem>
    ));
  }

  render() {
    const { onChange, value, items } = this.props;
    const selectedItem = _.find(items, item => item.value === value);
    return (
      <Downshift
        onChange={onChange}
        itemToString={i => _.get(i, 'label', 'no label')}
        selectedItem={selectedItem}
        render={(props) => {
          const {
            getInputProps,
            getItemProps,
            isOpen,
            inputValue,
            selectedItem,
            highlightedIndex,
          } = props;
          return (
            <div style={{ position: 'relative' }}>
              <Input {...getInputProps({ placeholder: 'Search area' })} />
              {isOpen ? (
                <Dropdown>
                  {this.renderSuggestions(
                    filterItems(items, inputValue),
                    { getItemProps, inputValue, selectedItem, highlightedIndex }
                  )}
                </Dropdown>
              ) : null}
            </div>
          );
        }}
      />
    );
  }
});
