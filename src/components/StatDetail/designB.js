import React from 'react';
import _ from 'lodash';
import styled from 'styled-components';
import numeral from 'numeral';
import * as Color from '../Color';
import { P, H5, H6 } from '../Typography';

const MetaItem = styled.div`
  flex: 1 0 25%;
  padding: 6px 0 0;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;


const MainInfo = styled.div`
  text-align: right;
  border-bottom: 1px solid #d8d8d8;
  padding-bottom: 4px;
`;

const MetaInfo = styled.div`
  display: flex;
`;

const PriceRow = styled(P)`
  color: #787878;
  margin-bottom: 0;
`;

const Text = styled.span`
  font-size: 11px;
  color: rgba(0, 0, 0, 0.54);
  text-align: center;
  ${props => props.value && `font-weight: bold;`}
`;

const EmText = styled.span`
  font-size: 20px;
  font-weight: bold;
  display: inline-block;
  margin-left: 20px;
`;
const MutedText = styled.span`
  font-size: 14px;
  color: #9a9a9a;
`;

const Container = styled.div`
  display: grid;
  grid-template-rows: 1fr 1fr 1fr;
  grid-auto-flow: column;

  div:nth-child(-n+3) {
    border-right: 1px solid #ebebeb;
  }
  div:nth-child(3n+1) {
    background-color: #ffffff;
    * {
      color: #666666;
    }
  }

  div:nth-child(3n+2) {
    background-color: #ed8747;
    * {
      color: #702b00;
    }
  }
  div:nth-child(3n+3) {
    background-color: #d4bd79;
    * {
      color: #4d3b08;
  }
  }
`;

const Info = styled.div`
  height: 100%;
  max-height: 36px;
  box-sizing: border-box;
  background-color: pink;
  padding: 8px 10px;

  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;

const TextPrice = styled(H5)`
  font-size: 0.75rem !important;
  text-transform: uppercase;
`;

const TextTitle = styled(H6)`
  font-size: 0.75rem !important;
`;

export default class extends React.PureComponent {
  
  render() {
    const { startingPersqm, yearComplete, salePrice = 0, capitalGain = 0, startingPrice = 0, rentPrice = 0, rentalYield = 0 } = this.props;
    return (
      <Container>
        <Info>
          <TextTitle>Starting Price (sq.m.)</TextTitle>
          <TextPrice>{_.isNaN(startingPrice) ? 'N/A' : `฿ ${numeral(startingPersqm).format('0,0a')}`}</TextPrice>
        </Info>
        <Info>
          <TextTitle>Sale (sq.m.)</TextTitle>
          <TextPrice>{_.isNaN(salePrice) ? 'N/A' : `฿ ${numeral(salePrice).format('0,0a')}`}</TextPrice>
        </Info>
        <Info>
          <TextTitle>Rent (sq.m.)</TextTitle>
          <TextPrice>{_.isNaN(rentPrice) ? 'N/A' : `฿${numeral(rentPrice).format('0,0.00')}`}</TextPrice>
        </Info>
        <Info>
          <TextTitle>Starting Price</TextTitle>
          <TextPrice>{_.isNaN(startingPrice) ? 'N/A' : `฿ ${numeral(startingPrice).format('0,0a')}`}</TextPrice>
        </Info>
        <Info>
          <TextTitle>Capital Gain</TextTitle>
          <TextPrice>{_.isNaN(capitalGain) ? 'N/A' : numeral(capitalGain).format('0.00%')}</TextPrice>
        </Info>
        <Info>
          <TextTitle>Rental Yield</TextTitle>
          <TextPrice>{_.isNaN(rentalYield) ? 'N/A' : numeral(rentalYield).format('0.00%')}</TextPrice>
        </Info>
      </Container>
    );
  }
}
