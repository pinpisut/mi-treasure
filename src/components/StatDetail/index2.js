import React from 'react';
import styled from 'styled-components';
import numeral from 'numeral';

const Row = styled.div`
  display: flex;
  flex-direction: row;
`;

const Col = styled.div`
  flex-grow: 1;
  text-align: ${props => props.number ? 'right' : 'left'};
`;

const Heading = styled.span`
  font-size: 11px;
`;

const Text = styled.span`
  font-size: 11px;
  color: rgba(0, 0, 0, 0.54);
  text-align: left;
`;

export default class extends React.PureComponent {
  
  render() {
    const { salePrice = 0, capitalGain = 0, startingPrice = 0, rentPrice = 0, rentalYield = 0 } = this.props;
    return (
      <Row>
        <Col style={{ paddingRight: 10 }}>
          <Heading>Price</Heading>
          <Row>
            <Col><Text>Avg. Price/sq.m.</Text></Col>
            <Col number><Text>{numeral(salePrice).format('0,0.00')}</Text></Col>
          </Row>
          <Row>
            <Col><Text>Capital Gain</Text></Col>
            <Col number><Text>{numeral(capitalGain).format('0.00%')}</Text></Col>
          </Row>
          <Row>
            <Col><Text>Starting Price</Text></Col>
            <Col number><Text>{numeral(startingPrice).format('0,0.00')}</Text></Col>
          </Row>
        </Col>
        <Col style={{ paddingLeft: 10 }}>
          <Heading>Rental Price</Heading>
          <Row>
            <Col><Text>Avg. Rent/sq.m.</Text></Col>
            <Col number><Text>{numeral(rentPrice).format('0,0.00')}</Text></Col>
          </Row>
          <Row>
            <Col><Text>Rental Yield</Text></Col>
            <Col number><Text>{numeral(rentalYield).format('0.00%')}</Text></Col>
          </Row>
        </Col>
      </Row>
    );
  }
}
