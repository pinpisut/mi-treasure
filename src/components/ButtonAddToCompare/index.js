import React from 'react';
import IconButton from '@material-ui/core/IconButton';
import IconFavorite from '@material-ui/icons/Favorite';

export default class extends React.PureComponent {
  handleClick = e => {
    this.props.onClick(this.props);
  }

  render() {
    const { active } = this.props;
    return (
      <IconButton
        {...this.props}
        onClick={this.handleClick}
      >
        {/* <IconFavorite style={{ color: active ? '#ff0000' : '#d7d7d7' }} /> */}
        <span className="icon-compare" style={{ color: active ? '#EC7024' : '' }} />
      </IconButton>
    );
  }
}