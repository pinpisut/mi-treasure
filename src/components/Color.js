export const primary = '#ec7024';

export const COLOR_YEAR_FUTURE = '#FDE47D';
export const COLOR_THIS_YEAR = '#51B576';
export const COLOR_YEAR_0_5 = '#FFAB65';
export const COLOR_YEAR_5_10 = '#AED8E3';
export const COLOR_YEAR_10UP = '#CBCBCB';

export const Tag_Article = '#184d75';
export const Tag_Review = '#e09fa3';

export const Comp_Price = '#075294';
export const Comp_Rental = '#8c0794';
export const Comp_CapGain = '#940752';
export const Comp_Yield = '#079494';

export const Tab_Hide = '#f2975f';



