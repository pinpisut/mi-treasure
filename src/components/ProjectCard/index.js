import React from 'react';
import _ from 'lodash';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import IconVisibility from '@material-ui/icons/Visibility';
import IconVisibilityOff from '@material-ui/icons/VisibilityOff';

import { addToCompare, removeFromCompare } from '../../actions/compare-actions';

import { H3, H6 } from '../Typography';
import ButtonAddToCompare from '../ButtonAddToCompare';
import StatDetail from '../StatDetail';
import { getProjectStat } from '../../utils/stat-utils';
import { getDistance } from '../../utils/map-utils';

import { getColorByYearComplete } from '../../screens/MapScreen/utils';

const PRINT = 'print';

const ViewToggleButton = (props) => (
  <IconButton {..._.omit(props, ['active'])}>
    {props.active ? <IconVisibility /> : <IconVisibilityOff style={{ color: '' }} />}
  </IconButton>
);

const LinkButton = (props) => (
  <IconButton {...props}>
    <span className="icon-link" />
  </IconButton>
)

const ReferenceBox = styled.div`
  background: ${props => props.color || '#ececec'};
  border-radius: 0 8px 8px 0;
  text-align: center;
  width: 30px;
  height: 40px;
  line-height: 40px;
`;

const SummaryPane = styled.div`
  display: flex;
  margin-left: -24px;
  margin-right: -24px;

  @media ${PRINT} {
    margin-bottom: -50px;
  }
`;

const ActionBar = styled.div`
  display: flex;
  flex-basis: 50;
  position: relative;
  top: -12px;

  @media ${PRINT} {
    display: none;
  }
`;

const mapStateToProps = (state, ownProps) => {
  const compareProjectIds = _.get(state, 'domain.compare.ids');
  return {
    projectAdddedToCompare: _.some(compareProjectIds, id => id === ownProps.id),
  };
}

const mapDispatchToProps = dispatch => {
  return {
    onClickCompare: (project) => {
      console.log('onClickCompare', project);
      if (!project.active) {
        dispatch(addToCompare(project.id));
      } else {
        dispatch(removeFromCompare(project.id));
      }
    }
  };
}


export default connect(mapStateToProps, mapDispatchToProps)(class extends React.PureComponent {
  
  handleViewToggle = (e) => {
    e.preventDefault();
    const { id, checked, onChange } = this.props;
    onChange(id, !checked);
  }
  render() {
    const { id, reference, title, location, checked, pointOfInterest, projectAdddedToCompare, onClickCompare } = this.props;
    
    const lat = _.get(pointOfInterest, 'location.position._lat');
    const lng = _.get(pointOfInterest, 'location.position._long');
    
    const projLat = _.get(location, 'position._lat');
    const projLng = _.get(location, 'position._long');

    const linkToAgent = _.get(this.props, 'integrations.TheAgent.ProjectLink');
    
    const distance = getDistance(projLat, projLng, lat, lng);
    const stat = getProjectStat(this.props);
    return (
      <Card elevation={0} style={{ background: 'white', boxShadow: '0 5px 10px 0 rgba(0, 0, 0, 0.05)' }}>
        <CardContent style={{ paddingBottom: 12 }}>
          <SummaryPane>
            <div style={{ flexBasis: 48 }}>
              <ReferenceBox color={getColorByYearComplete(stat.yearComplete)}>{reference}</ReferenceBox>
            </div>
            <div style={{ flexGrow: 1 }}>
              <H3 style={{ fontSize: 18, marginBottom: 3 }}>{_.get(title, 'en', _.toString(title))}</H3>
              <H6 style={{ fontSize: 14, opacity: 0.7 }}>{Math.round(distance * 1000)}m from {_.get(pointOfInterest, 'title.en')}</H6>
            </div>
            <ActionBar>
              {!_.isEmpty(linkToAgent) &&
                <Tooltip placement="top" title="Link to TheAgent">
                  <LinkButton component={Link} target="_blank" to={linkToAgent} />
                </Tooltip>
              }
              <Tooltip placement="top" title={checked ? 'Hide on Map' : 'Show on Map'}>
                <ViewToggleButton active={!checked} onClick={this.handleViewToggle} />
              </Tooltip>
              <Tooltip placement="top" title={!projectAdddedToCompare ? 'Add to Compare' : 'Remove from Compare'}>
                <ButtonAddToCompare onClick={onClickCompare} id={id} active={projectAdddedToCompare} />
              </Tooltip>
            </ActionBar>
          </SummaryPane>
          <StatDetail {...stat} />
        </CardContent>
      </Card>
    )
  }
});
