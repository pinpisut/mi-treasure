import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';

import { H1, H3 } from '../Typography';
import * as Color from '../Color';

const Container = styled.div`
  flex: 1;
  text-align: center;
  border: 1px solid #f7f7f7;
  padding: 10px;
`;

const Label = styled.div``;
const Value = styled.div``;

const StyledH1 = styled(H1)`
  color: ${Color.primary};
  color: black;
`;

export default connect(
  state => ({
    position: state.domain.design.overviewPosition,
  })
)(({ label, value, position }) => (
  <Container>
    <Label><H3 style={{ color: position === 'B' ? '#787878' : '#000' }}>{label}</H3></Label>
    <Value><StyledH1>{value}</StyledH1></Value>
  </Container>
));