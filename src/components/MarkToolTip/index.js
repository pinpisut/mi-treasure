import React from 'react';
import _ from 'lodash';
import Color from 'color';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';
import IconVisibility from '@material-ui/icons/Visibility';
import IconVisibilityOff from '@material-ui/icons/VisibilityOff';

import numeral from 'numeral';

import ButtonAddToCompare from '../ButtonAddToCompare';
import { P } from '../Typography';

import { getProjectStat } from '../../utils/stat-utils';
import { getColorByYearComplete } from '../../screens/MapScreen/utils';

const Detail = styled(P)`
  margin: 0px !important;
  margin-right: 20px !important;
  white-space: nowrap;
`;

const ViewToggleButton = props => (
  <IconButton {..._.omit(props, ['active'])}>
    {props.active ? (
      <IconVisibility />
    ) : (
      <IconVisibilityOff style={{ color: '' }} />
    )}
  </IconButton>
);

const LinkButton = props => (
  <IconButton {...props}>
    <span className='icon-link' />
  </IconButton>
);

const ProjectNotShowDetail = styled.div`
  display: flex;
  white-space: nowrap;
  overflow: hidden;
  font-size: 0.6rem;
  margin: 5px;
`;

const ActionBarPopover = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 40px;
  position: absolute;
  top: 50%;
  right: -45px;
  background-color: white;
  border-radius: 4px;
  box-shadow: 0 0 5px rgba(0, 0, 0, 0.25);
`;

const ActionBarProjectShowDetail = styled.div`
  display: flex;
  justify-content: flex-end;
`;

const HeaderActionBarProjectShowDetail = styled.div`
  display: flex;
  flex-direction: column-reverse;
  border-bottom: 1px solid
    ${props =>
      Color(props.color)
        .darken(0.2)
        .string() || 'black'};
  padding: 8px 0px;
  magin-bottom: 8px;
`;

const detailLabels = {
  yearComplete: { label: 'Year Completed', prefix: '', format: '0' },
  salePrice: { label: 'Avg. price/sq.m.', prefix: '฿ ', format: '0,0a' },
  rentPrice: { label: 'Avg. rent/sq.m.', prefix: '฿ ', format: '0,0.00' },
  capitalGain: { label: 'Capital gain', prefix: '', format: '0.00%' },
  rentalYield: { label: 'Rental Yield', prefix: '', format: '0.00%' }
};

export default class extends React.PureComponent {
    static defaultProps = {
      onValueProject: () => null,
    };
   

    render() {
      const {
        viewOptionList,
        checked,
        project,
        compareProjectIds,
        showDetail,
        isHover
      } = this.props;

      const projectStat = getProjectStat(project);
      const linkToAgent = _.get(project, 'integrations.TheAgent.ProjectLink');
      const projectAdddedToCompare = _.some(
        compareProjectIds,
        proId => proId === project.id
      );

      if (showDetail) {
        return (
          <div style={{ minWidth: 200 }}>
          <HeaderActionBarProjectShowDetail
            color={getColorByYearComplete(project.yearComplete)}
          >
            <P style={{ fontWeight: 'bold', margin: 0 }}>{project.title.en}</P>
            <ActionBarProjectShowDetail>
              {!_.isEmpty(linkToAgent) && (
                <Tooltip placement='top' title='Link to TheAgent'>
                  <LinkButton
                    style={{
                      width: 15,
                      height: 15,
                      fontSize: 20,
                      margin: '4px 8px'
                    }}
                    component={Link}
                    target='_blank'
                    to={linkToAgent}
                  />
                </Tooltip>
              )}
              <Tooltip
                placement='top'
                title={checked ? 'Hide on Map' : 'Show on Map'}
              >
                <ViewToggleButton
                  style={{ width: 15, height: 15, margin: '4px 8px' }}
                  active={!checked}
                  onClick={() => this.props.onValueProject('theEye', project.id)}
                />
              </Tooltip>
              <Tooltip
                placement='top'
                title={
                  !projectAdddedToCompare
                    ? 'Add to Compare'
                    : 'Remove from Compare'
                }
              >
                <ButtonAddToCompare
                  style={{
                    width: 15,
                    height: 15,
                    fontSize: 20,
                    margin: '4px 8px'
                  }}
                  onClick={() => this.props.onValueProject('compare', project.id)}
                  active={projectAdddedToCompare}
                />
              </Tooltip>
            </ActionBarProjectShowDetail>
          </HeaderActionBarProjectShowDetail>
          <table>
            <tbody>
              {viewOptionList.map(item => (
                <tr key={item}>
                  <td>
                    <Detail>{detailLabels[item].label}</Detail>
                  </td>
                  <td>
                    <Detail>
                      {_.get(detailLabels, `${item}.prefix`)}
                      {numeral(projectStat[item]).format(
                        _.get(detailLabels, `${item}.format`, '0,0.00')
                      )}
                    </Detail>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
        );
      }

      
      if (isHover) {
        return (
          <ProjectNotShowDetail>
            <P style={{ margin: 0 }}>{project.title.en}</P>
            <ActionBarPopover>
              {!_.isEmpty(linkToAgent) && (
                <Tooltip placement='bottom' title='Link to TheAgent'>
                  <LinkButton
                    style={{
                      width: '100%',
                      height: 25,
                      fontSize: 20,
                      margin: '10px',
                      borderRadius: 0
                    }}
                    component={Link}
                    target='_blank'
                    to={linkToAgent}
                  />
                </Tooltip>
              )}
              <Tooltip
                placement='bottom'
                title={
                  !projectAdddedToCompare
                    ? 'Add to Compare'
                    : 'Remove from Compare'
                }
              >
                <ButtonAddToCompare
                  style={{
                    width: '100%',
                    height: 25,
                    fontSize: 20,
                    marginBottom: '10px',
                    borderRadius: 0
                  }}
                  onClick={() => this.props.onValueProject('compare', project.id)}
                  active={projectAdddedToCompare}
                />
              </Tooltip>
            </ActionBarPopover>
          </ProjectNotShowDetail>
        );
      }

      return <div />
    }
  };
