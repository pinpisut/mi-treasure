import React, { Component } from "react";
import styled, { css } from "styled-components";
import Button from "@material-ui/core/Button";
import _ from "lodash";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

import Container from "./styled.js";

const StyledButton = styled(Button)`
  &&& {
    color: white;
    border: transparent;
    background: transparent;
    border: 1px solid white;
    padding: 8px 12px;
    margin-right: 30px;

    @media (max-width: 1302px) {
      margin-right: 10px;
    }

    ${props =>
      props.active &&
      css`
        border: 1px solid white;
        background: #eb6323;
      `};
  }
`;

const TabIcon = styled.span`
  margin-right: 3px;
  font-size: 1.3em;
  position: relative;
  top: -1px;
`;

const mapStateToProps = (state, ownProps) => {
  const projectEntities = _.get(state, "entities.projects.entities");

  const mapReducer = _.get(state, "domain.map");
  const { result } = mapReducer;
  const visibleProjects = _.map(result, pId => _.get(projectEntities, pId));
  const sizeProjects = _.size(visibleProjects);

  return {
    ...ownProps,
    sizeProjects
  };
};

class ContentTab extends Component {

  static defaultProps = {
    poi: '',
    page: '',
    open: null,
    onClickContentTab: () => null
  };

  state = {
    hideTab: true,
  };

  handleClickTab = (tab) => {
    this.setState({ hideTab: false }, () => {
      this.props.onClickContentTab(this.state.hideTab);
    });
  };

  render() {
    const { sizeProjects, poi, open, page } = this.props;
    return (
      <Container>
        <Link to={`/map/${poi}/projects`}>
          <StyledButton
            active={page === "projects" && open}
            onClick={() => this.handleClickTab('Projects')}
          >
            <TabIcon className="icon-tab-project" />Projects ({sizeProjects})
          </StyledButton>
        </Link>
        <Link to={`/map/${poi}/facilities`}>
          <StyledButton
            active={page === "facilities" && open}
            onClick={() => this.handleClickTab('Facilities')}
          >
            <TabIcon className="icon-tab-facility" />Facilities
          </StyledButton>
        </Link>
        <Link to={`/map/${poi}/analysis`}>
          <StyledButton
            active={page === "analysis" && open}
            onClick={() => this.handleClickTab('Analysis')}
          >
            <TabIcon className="icon-tab-analysis" />Analysis
          </StyledButton>
        </Link>
      </Container>
    );
  }
}

export default connect(mapStateToProps)(ContentTab);
