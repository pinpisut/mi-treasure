import React from "react";
import PT from "prop-types";
import styled, { css } from "styled-components";
import numeral from "numeral";
import _ from "lodash";
import { withStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import * as Color from "../../components/Color";
import { H5, H6 } from "../../components/Typography";

const styles = theme => ({
  root: {
    width: "100%",
    borderRadius: 4,
    border: "solid 1px #dedede",
    backgroundColor: "#ffffff",
    boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.05)",
    display: "flex",
    flexDirection: "row",
    alignItems: "flex-start"
  }
});
const WrapTableHead = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1 0 195px;
  border-right: 1px solid #dedede;
  tr:nth-child(odd) {
    background-color: rgba(232, 232, 232, 0.5);
  }
  tr:nth-child(1) {
    background-color: #fff;
  }
`;

const WrapTable = styled.div`
  width: 100%;
  max-width: 938px;
  display: flex;
  flex-direction: row;
  overflow-x: auto;
`;

const TR = styled.div`
  height: ${props => (props.box ? "100px" : "80px")};
  border-bottom: ${props => (props.top ? "1px solid #dedede" : "1px solid #dedede")};
  display: flex;
  flex-direction: row;
  border-bottom: 1px solid #dedede;
`;

const baseCell = css`
  border-left: 1px solid #dedede;
  height: 100%;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  flex: 1 0 135px;
`;

const TH = styled.div`
  ${baseCell}
  padding-left: ${props => (props.center ? "unset" : "9px")};
  justify-content: ${props => (props.center ? "center" : "unset")};
  margin-left: -1px;
`;

const TD = styled.div`
  ${baseCell};
  align-items: ${props => props.alignItems};
  border-left: ${props => props.borderLeft ? '1px solid #dedede' :  'none'};

`;

const Title = styled(H5)`
  margin: 0 !important;
`;

const Data = styled(H6)`
  margin: 0 !important;
  font-size: 0.75rem !important;
`;

const BoxValue = styled.div`
  width: 80px;
  height: ${props => props.height}px;
  background-color: ${props => props.color};
`;

const TC = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1 0 auto;
`;

class Comparetable extends React.PureComponent {
  static propTypes = {
    classes: PT.object.isRequired,
    heads: PT.arrayOf(PT.object),
    selectedList: PT.arrayOf(PT.object)
  };
  static defaultProps = {
    heads: [
      {
        id: "title",
        data: "Project Name"
      },
      {
        id: "avgPpsqm",
        data: "Price/Sq.m."
      },
      {
        id: "avgRpsqm",
        data: "Rental Price/Sq.m."
      },
      {
        id: "capitalGain",
        data: "Capital Gain"
      },
      {
        id: "rentalYield",
        data: "Rental Yield"
      },
      {
        id: "yearOpen",
        data: "Year Open"
      },
      {
        id: "yearComplete",
        data: "Year Complete"
      },
      {
        id: "startingPrice",
        data: "Starting Price"
      },
      // {
      //   id: "avgPrice",
      //   data: "Average Price (MB)"
      // },
      // {
      //   id: "avgRental",
      //   data: "Average Rental Price (KB)"
      // },
     
    ],
    selectedList: [
      {
        id: "0",
        title: "Ideo",
        location: "Bangna",
        yearOpen: "2012",
        yearComplete: "2012",
        avgPrice: "23 MB",
        avgRental: "22KB",
        avgPpsqm: 1000,
        avgRpsqm: 2500,
        capitalGain: 4,
        rentalYield: 9
      },
      {
        id: "1",
        title: "Ideo two",
        location: "Bangna",
        yearOpen: "2012",
        yearComplete: "2012",
        avgPrice: "23 MB",
        avgRental: "22KB",
        avgPpsqm: 1500,
        avgRpsqm: 700,
        capitalGain: 4,
        rentalYield: 9
      },
      {
        id: "2",
        title: "Ideo two",
        location: "Bangna",
        yearOpen: "2012",
        yearComplete: "2012",
        avgPrice: "23 MB",
        avgRental: "22KB",
        avgPpsqm: 2500,
        avgRpsqm: 1300,
        capitalGain: 4,
        rentalYield: 27
      },
      {
        id: "3",
        title: "Ideo two",
        location: "Bangna",
        yearOpen: "2012",
        yearComplete: "2012",
        avgPrice: "23 MB",
        avgRental: "22KB",
        avgPpsqm: 2500,
        avgRpsqm: 1300,
        capitalGain: 4,
        rentalYield: 27
      }
    ]
  };
  constructor(props) {
    super(props);
    this.state = {};
  }

  generateHeight = (type, num) => {
    const maxValueItem = _.maxBy(this.props.selectedList, item => _.toNumber(_.get(item, type, 0)));
    const maxValue = _.get(maxValueItem, type, 0);
    console.log('maxValue', maxValue, type, num, this.props.selectedList);
    return 60 * (num / maxValue);
  };
  getColorText = type => {
    switch (type) {
      case "avgPpsqm":
        return Color.Comp_Price;
      case "avgRpsqm":
        return Color.Comp_Rental;
      case "capitalGain":
        return Color.Comp_CapGain;
      case "rentalYield":
        return Color.Comp_Yield;
      default:
        return "#303030";
    }
  };

  renderBox = (type, value, index) => {
    let val = _.isNaN(value) ? 0 : value;
    console.log("type", type, value, val);
    if (_.includes(["capitalGain", "rentalYield"], type)) {
      val = `${numeral(value).format("0.00%")}`;
    }
    if (type === "avgPpsqm") {
      val = `฿ ${numeral(value).format("0,0a")}`;
    }
    if (type === "avgRpsqm") {
      val = `฿ ${numeral(value).format("0,0.00")}`;
    }

    return (
      <TD key={index} alignItems="flex-end" borderLeft={false}>
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
            paddingTop: 10,
          }}
        >
          <Data style={{ color: this.getColorText(type) }}>{val} </Data>
          <BoxValue
            color={this.getColorText(type)}
            height={this.generateHeight(type, _.isNaN(value) ? 0 : value)}
          />
        </div>
      </TD>
    );
  };

  renderColumn = (col, index) => {
    return (
      <TC>
        <TR top>
          <TH key={index} center style={{ flexDirection: "column" }}>
            <Title style={{ color: this.getColorText() }}>{col.title}</Title>
            <Data>{col.location}</Data>
          </TH>
        </TR>
        <TR box>{this.renderBox("avgPpsqm", col.avgPpsqm, index)}</TR>
        <TR box>{this.renderBox("avgRpsqm", col.avgRpsqm, index)}</TR>
        <TR box>{this.renderBox("capitalGain", col.capitalGain, index)}</TR>
        <TR box>{this.renderBox("rentalYield", col.rentalYield, index)}</TR>
        <TR>
          <TD key={index} alignItems="center" borderLeft>
            <Data style={{ color: this.getColorText("yearOpen") }}>
              {col.yearOpen}
            </Data>
          </TD>
        </TR>
        <TR>
          <TD key={index} alignItems="center" borderLeft>
            <Data style={{ color: this.getColorText("yearComplete") }}>
              {col.yearComplete}
            </Data>
          </TD>
        </TR>
        <TR>
          <TD key={index} alignItems="center" borderLeft>
            <Data style={{ color: this.getColorText("startingPrice") }}>
              {col.startingPrice}
            </Data>
          </TD>
        </TR>
        
      </TC>
    );
  };

  render() {
    const { classes, heads, selectedList } = this.props;

    return (
      <Paper className={classes.root}>
        <WrapTableHead>
          {_.map(heads, (item, index) => (
            <TR
              key={index}
              top={item.id === "title"}
              box={_.includes(
                ["avgPpsqm", "avgRpsqm", "capitalGain", "rentalYield"],
                item.id
              )}
            >
              <TH style={{ width: 202 }}>
                <Title style={{ color: this.getColorText(item.id) }}>
                  {item.data}
                </Title>
              </TH>
            </TR>
          ))}
        </WrapTableHead>
        <WrapTable>
          {_.map(selectedList, (col, index) => this.renderColumn(col))}
          {
            // <TR top>
            //   {_.map(selectedList, (item, index) => (
            //     <TH
            //       key={index}
            //       center
            //       style={{
            //         flexDirection: "column"
            //       }}
            //     >
            //       <Title style={{ color: this.getColorText() }}>
            //         {item.title}
            //       </Title>
            //       <Data>{item.location}</Data>
            //     </TH>
            //   ))}
            // </TR>
            // <TR>
            //   {_.map(selectedList, (item, index) => (
            //     <TD key={index} alignItems="center">
            //       <Data style={{ color: this.getColorText("yearOpen") }}>
            //         {item.yearOpen}
            //       </Data>
            //     </TD>
            //   ))}
            // </TR>
            // <TR>
            //   {_.map(selectedList, (item, index) => (
            //     <TD key={index} alignItems="center">
            //       <Data style={{ color: this.getColorText("yearComplete") }}>
            //         {item.yearComplete}
            //       </Data>
            //     </TD>
            //   ))}
            // </TR>
            // <TR>
            //   {_.map(selectedList, (item, index) => (
            //     <TD key={index} alignItems="center">
            //       <Data style={{ color: this.getColorText("startingPrice") }}>
            //         {item.startingPrice}
            //       </Data>
            //     </TD>
            //   ))}
            // </TR>
          }
          {
            //  <TR>
            //   {_.map(selectedList, item => (
            //     <TD alignItems="center">
            //       <Data style={{ color: this.getColorText("avgPrice") }}>
            //         {item.avgPrice}
            //       </Data>
            //     </TD>
            //   ))}
            // </TR>
            // <TR>
            //   {_.map(selectedList, item => (
            //     <TD alignItems="center">
            //       <Data style={{ color: this.getColorText("avgRental") }}>
            //         {item.avgRental}
            //       </Data>
            //     </TD>
            //   ))}
            // </TR>
          
          // <TR box>
          //   {_.map(selectedList, (item, index) =>
          //     this.renderBox("avgPpsqm", item.avgPpsqm, index)
          //   )}
          // </TR>
          // <TR box>
          //   {_.map(selectedList, (item, index) =>
          //     this.renderBox("avgRpsqm", item.avgRpsqm, index)
          //   )}
          // </TR>
          // <TR box>
          //   {_.map(selectedList, (item, index) =>
          //     this.renderBox("capitalGain", item.capitalGain, index)
          //   )}
          // </TR>
          // <TR box>
          //   {_.map(selectedList, (item, index) =>
          //     this.renderBox("rentalYield", item.rentalYield, index)
          //   )}
          // </TR>
          }
        </WrapTable>
      </Paper>
    );
  }
}

export default withStyles(styles)(Comparetable);
