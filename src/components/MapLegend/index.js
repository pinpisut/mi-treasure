import React from 'react';
import styled from 'styled-components';
import _ from 'lodash';
import { connect } from 'react-redux';
import Hidden from '@material-ui/core/Hidden';
import Typography from '@material-ui/core/Typography';
import Collapse from '@material-ui/core/Collapse';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import Popover from '@material-ui/core/Popover';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import Checkbox from '@material-ui/core/Checkbox';

import Card from '@material-ui/core/Card';

const BREAKPOINT = 1200;

const projectCheckboxes = [
  { value: 'yearComplete', label: 'Year Complete' },
  { value: 'salePrice', label: 'Sale price/sq.m.' },
  { value: 'rentPrice', label: 'Rent price/sq.m.' },
  { value: 'capitalGain', label: 'Capital gain' },
  { value: 'rentalYield', label: 'Rental Yield' },
];

const Container = styled.div`
  position: absolute;
  bottom: 10px;
  left: 50%;
  transform: translateX(-50%);
  display: flex;
  justify-content: flex-start;
  flex-wrap: nowrap;
  transition: all .6s ease-out;

  @media print {
    display: none;
  }
`;


const LegendWrapper = styled.div`
  font-size: 11px;
  color: #787878;
  margin: 0 16px;
  display: flex;
  flex-direction: row;
  align-items: center;
  flex-wrap: nowrap;
  white-space: nowrap;
`;

const ColorBox = styled.div`
  display: inline-block;
  width: 14px;
  height: 14px;
  margin-right: 14px;
  background: ${props => props.color || '#ececec'};
`;


const Legend = ({ label, color }) => (
  <LegendWrapper>
    <ColorBox color={color} />
    <Typography style={{ fontSize: 12 }}>{label}</Typography>
  </LegendWrapper>
)

const ListLegendWrapper = styled.div`
  display: block;
  @media (min-width: ${BREAKPOINT}px) {
    display: none;
  }
`;

const ListLegendWrapperDesktop = styled.div`
  display: none;
  @media (min-width: ${BREAKPOINT}px) {
    display: flex;
    flex-direction: row;
    min-width: 600px;
  }
`;

const mapStateToProps = (state, ownProps) => {
  return {
    ...ownProps,
    layout: state.domain.design.legend,
    viewOptions: state.domain.map.viewOptionList,
  }
}

export default connect(
  mapStateToProps,
)(class extends React.Component {

  static getDerivedStateFromProps (nextProps, prevState) {
    return {
      checked: nextProps.viewOptions,
    }
  }
  state = {
    open: false,
    checked: [],

    // layout B
    openPopover: false,
  }

  handleClick = () => {
    this.setState({ open: !this.state.open });
  }

  handleToggle = value => () => {
    const { dispatch } = this.props;
    const { checked } = this.state;
    const currentIndex = checked.indexOf(value);
    const newChecked = [...checked];

    if (currentIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currentIndex, 1);
    }

    this.setState({
      checked: newChecked,
    });
    
    dispatch({
      type: 'DOMAIN/MAP/VIEWOPTION/CHANGE',
      values: newChecked,
    });
  };

  openPopover = (e) => {
    this.setState({
      openPopover: true,
    })
  }

  handleClosePopover = (e) => {
    this.setState({
      openPopover: false,
    })
  }

  render() {
    const { layout, yearRanges } = this.props;
    const { checked } = this.state;

    if (layout === 'A') {
      return (
        <Container>
          <Card elevation={12}>
            <div>
              <List dense disablePadding>
                {_.map(yearRanges, range => <ListItem><Legend label={range.label} color={range.color} /></ListItem>)}
                <Divider />
                <ListItem button style={{ background: '#f7f7f7' }} onClick={this.handleClick}>
                  <ListItemText primary="View Option" />
                  {this.state.open ? <ExpandLess /> : <ExpandMore />}
                </ListItem>
                <Collapse in={this.state.open} timeout="auto" unmountOnExit>
                  <List component="div" disablePadding dense style={{ background: '#f7f7f7' }}>
                    {projectCheckboxes.map(item => (
                      <ListItem
                        button
                        key={item.value}
                        style={{ paddingTop: 8, paddingBottom: 8, paddingLeft: 48 }}
                        onClick={this.handleToggle(item.value)}
                      >
                        <Checkbox
                          checked={checked.indexOf(item.value) !== -1}
                          tabIndex={-1}
                          disableRipple
                        />
                        <ListItemText
                          primary={item.label}
                          style={{ fontSize: 12 }}
                        />
                      </ListItem>
                    ))}
                  </List>
                </Collapse>
              </List>
            </div>
          </Card>
        </Container>
      );
    }

    if (layout === 'B') {
      return (
        <Container>
          <Paper elevation={12} style={{ background: 'white', display: 'flex' }}>
            <ListLegendWrapperDesktop>{_.map(yearRanges, range => <ListItem dense disableGutters><Legend label={range.label} color={range.color} /></ListItem>)}</ListLegendWrapperDesktop>
            <Button
              buttonRef={c => this.anchorEl = c}
              onClick={this.openPopover}
              style={{ fontSize: '0.8rem', whiteSpace: 'nowrap', width: 100, flexShrink: 0, border: '1px solid #eb6323', color: '#eb6323'}}
            >View Option</Button>
            <Popover
              open={this.state.openPopover}
              anchorEl={this.anchorEl}
              onClose={this.handleClosePopover}
              anchorOrigin={{
                vertical: 'top',
                horizontal: 'right',
              }}
              transformOrigin={{
                vertical: 'bottom',
                horizontal: 'right',
              }}
            >
              <List component="div" disablePadding dense style={{ background: '#f7f7f7' }}>
                <ListLegendWrapper>
                  {_.map(yearRanges, range => <ListItem dense disableGutters><Legend label={range.label} color={range.color} /></ListItem>)}
                  <Divider />
                </ListLegendWrapper>
                {projectCheckboxes.map(item => (
                  <ListItem
                    button
                    key={item.value}
                    style={{ paddingTop: 8, paddingBottom: 8 }}
                    onClick={this.handleToggle(item.value)}
                  >
                    <Checkbox
                      checked={checked.indexOf(item.value) !== -1}
                      tabIndex={-1}
                      disableRipple
                    />
                    <ListItemText
                      primary={item.label}
                      style={{ fontSize: 12 }}
                    />
                  </ListItem>
                ))}
              </List>
            </Popover>
          </Paper>
        </Container>
      );
    }

    return (<div />);
  }
});
