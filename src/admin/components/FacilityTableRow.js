import React from 'react';
import _ from 'lodash';
import numeral from 'numeral';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import Button from '../../components/Button';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import IconOpenInNew from '@material-ui/icons/OpenInNew';
import { db } from '../../engine';

const StyledTableRow = styled(TableRow)`
  cursor: pointer;
  &:hover {
    background: #fafafa;
  }
`;

export default class extends React.PureComponent {
  state = {
    data: {
      title: '',
    }
  }
  componentDidMount() {
    const { id } = this.props;
    if (id) {
      const docRef = db.collection('facilities').doc(id);
      docRef.onSnapshot(doc => {
        const result = doc.data()
        const docRefType = db.collection('typeOfFacility').doc(`1`);
        docRefType.onSnapshot(docRefTypeData => {
          this.setState({
            data: {...result, type : docRefTypeData.data()},
          })
        });
      });
    }
  }
  render() {
    const { id, onClick } = this.props;
    const { data } = this.state;
    const { title } = data;
    return (
      <StyledTableRow hover onClick={onClick}>
        <TableCell style={{ color: '#AAA' }}>{id}</TableCell>
        <TableCell>{_.get(title, 'en', title)}</TableCell>
        <TableCell>{_.get(data, 'type.title.en', '')}</TableCell>
        <TableCell numeric>{_.get(data, 'location.lat', '')}</TableCell>
        <TableCell numeric>{_.get(data, 'location.lon', '')}</TableCell>
      </StyledTableRow>
    );
  }
}
