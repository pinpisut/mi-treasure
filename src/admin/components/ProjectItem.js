import React from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';

import { H5, P } from '../Typography';
import { db } from '../../engine';

export default class extends React.PureComponent {
  state = {
    data: {
      title: '',
    }
  }
  componentDidMount() {
    const { id } = this.props;
    if (id) {
      const docRef = db.collection('projects').doc(id);
      docRef.get().then(doc => {
        this.setState({
          data: doc.data(),
        })
      })

    }
  }
  render() {
    const { id } = this.props;
    const { data } = this.state;
    const { title } = data;
    return (
      <Card elevation={1}>
        <CardContent>
          <H5>{title}</H5>
          <P>Subtitle</P>
        </CardContent>
      </Card>
    );
  }
}
