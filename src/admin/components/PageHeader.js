import React from 'react';
import styled from 'styled-components';
import Typography from '@material-ui/core/Typography';

const Container = styled.div`
  align-items: center;
  padding: 12px 0 0;
  position: relative;
  min-height: 80px;
  margin-bottom: 24px;

  &:after {
    position: absolute;
    z-index: 1;
    bottom: 0;
    left: 0;
    right: 0;
    margin: 0 -40px 0;
    content: '';
    height: 1px;
    background: #ccc;
  }

  .action-bar {
    position: absolute;
    z-index: 2;
    height: 1px;
    right: 50px;
    bottom: 0;
    display: flex;
    align-items: center;
  }
`;

export default ({ title, renderActionButtons, children }) => (
  <Container>
    <Typography variant="headline">{title}</Typography>
    {children}
    {typeof renderActionButtons === 'function' && (
      <div className="action-bar">{renderActionButtons()}</div>
    )}
  </Container>
)