import React from 'react';

import Slide from '@material-ui/core/Slide';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Input from '@material-ui/core/Input';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';

import CircularProgress from '@material-ui/core/CircularProgress';

import { notify } from '../../components/Notification';
import { changePassword } from '../api/user';

const Transition = (props) => {
  return <Slide direction="up" {...props} />;
}

export default class extends React.PureComponent {
  static defaultProps = {
    onClose: () => null
  }

  state = {
    loading: false,
  }

  handleClose = (e) => {
    this.props.onClose(true);
  }

  handleCancel = (e) => {
    this.handleClose(e);
  }

  save = (e) => {
    const { uid } = this.props.user;
    const password = this.passwordInput.value;
    const rePassword = this.rePasswordInput.value;
    this.handleClose();
    this.setState({
      loading: true,
    });
    
    if (password !== rePassword) {
      notify.show(`Password does not match the confirm password`, 'error');
        this.setState({ loading: false });
        this.passwordInput.value = '';
        this.rePasswordInput.value = '';
    } else {
      changePassword(uid, password)
        .then(user => {
          notify.show(`Reset user ${user.email}'s password`, 'success');
          this.props.onUpdate(user);
        })
        .catch(error => notify.show(error.message, 'error'))
        .finally(() => this.setState({ loading: false }));
    }
  }

  render() {
    const { loading } = this.state;
    return (
        <Dialog
          open={this.props.open}
          TransitionComponent={Transition}
          keepMounted
          onClose={this.handleClose}
          aria-labelledby="alert-dialog-slide-title"
          aria-describedby="alert-dialog-slide-description"
        >
          <DialogTitle id="alert-dialog-slide-title">
            Reset Password
          </DialogTitle>
          <DialogContent>
            <div style={{ display: 'flex', flexDirection: 'column', width: 500, maxWidth: '100%' }}>
              <FormControl component="fieldset" required style={{ marginBottom: 12 }}>
                <FormLabel component="legend">Password</FormLabel>
                <Input
                  autoFocus
                  inputRef={node => this.passwordInput = node}
                  type="password"
                  fullWidth
                />
              </FormControl>
              <FormControl component="fieldset" required style={{ marginBottom: 12 }}>
                <FormLabel component="legend">Confirm Password</FormLabel>
                <Input
                  inputRef={node => this.rePasswordInput = node}
                  type="password"
                  fullWidth
                />
              </FormControl>
            </div>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleCancel} color="primary">
              Cancel
            </Button>
            <Button onClick={this.save} color="primary" disabled={loading}>
              {!loading ? 'Save' : <CircularProgress size={24} />}
            </Button>
          </DialogActions>
        </Dialog>
    );
  }
}