import React from 'react';
import _ from 'lodash';
import numeral from 'numeral';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import Button from '../../components/Button';
import Chip from '@material-ui/core/Chip';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import IconOpenInNew from '@material-ui/icons/OpenInNew';
import { db } from '../../engine';

const StyledTableRow = styled(TableRow)`
  cursor: pointer;
  &:hover {
    background: #fafafa;
  }
`;

export default class extends React.PureComponent {
  state = {
    data: {
      title: '',
    }
  }
  componentDidMount() {
    const { id } = this.props;
    if (id) {
      const docRef = db.collection('projects').doc(id);
      docRef.onSnapshot(doc => {
        this.setState({
          data: doc.data(),
        })
      });
    }
  }
  render() {
    const { id, onClick } = this.props;
    const { data } = this.state;
    const title = _.get(data, 'title', '');
    return (
      <StyledTableRow hover onClick={onClick}>
        <TableCell style={{ color: '#AAA' }}>{id}</TableCell>
        <TableCell>{_.get(title, 'en', title)}</TableCell>
        <TableCell>{_.get(data, 'integrations.TheAgent.ProjectID')}</TableCell>
        <TableCell numeric>{numeral(_.get(data, 'current.salePrice')).format('0,0a')}</TableCell>
        <TableCell numeric>{numeral(_.get(data, 'current.rentPrice')).format('0,0')}</TableCell>
        <TableCell>
          {_.get(data, 'integrations.TheAgent.ProjectLink') &&
            <Chip
              label={`ID: ${_.get(data, 'integrations.TheAgent.ProjectID')}`}
              target="_blank"
              component={Link}
              to={_.get(data, 'integrations.TheAgent.ProjectLink')}
              clickable
              onClick={e => e.stopPropagation()}
              onDelete={console.log}
              deleteIcon={<IconOpenInNew style={{ fontSize: '0.9em' }} />}
            />
            // <Button color="primary" variant="outlined" component={Link} target="_blank" to={_.get(data, 'integrations.TheAgent.ProjectLink')}>
            //   <span style={{ whiteSpace: 'nowrap' }}>ID: {_.get(data, 'integrations.TheAgent.ProjectID')}</span> <IconOpenInNew style={{ fontSize: '0.875rem', marginLeft: 6 }} />
            // </Button>
          }
        </TableCell>
      </StyledTableRow>
    );
  }
}
