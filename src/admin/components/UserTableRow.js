import React from 'react';
import moment from 'moment';
import _ from 'lodash';

import IconButton from '@material-ui/core/IconButton';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import Chip from '@material-ui/core/Chip';
import Avatar from '@material-ui/core/Avatar';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';

import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';

import ResetPasswordDialog from './ResetPasswordDialog';
import EditProfileDialog from './EditProfileDialog';
import { notify } from '../../components/Notification';
import { changePassword, disableUser, enableUser, deleteUser } from '../api/user';
import { CircularProgress } from '@material-ui/core';

import db from "../../engine/db";
import { Tab_Hide } from '../../components/Color';

export default class extends React.PureComponent {
  state = {
    loading: false,
    openMenu: false,
  }

  componentDidMount(){
    const { uid } = this.props.user;
    const data = db.collection("users").doc(uid);

    data.onSnapshot(doc => {
      const name = _.get(doc.data(), 'name')
      const phoneNumber = _.get(doc.data(), 'phoneNumber')
      const company = _.get(doc.data(), 'company')
      this.setState({
        name,
        phoneNumber,
        company
      })
    })
  }

  handleClick = (event, uid) => {
    this.setState({
      openMenu: true,
      openEditProfile: false,
      openResetPassword: false,
    });
  }

  handleCloseMenu = () => {
    this.setState({ openMenu: false });
  }

  handleDeleteUser = (e) => {
    const { uid } = this.props.user;
    this.handleCloseMenu();
    this.setState({ loading: true });
    deleteUser(uid)
      .then(() => {
        notify.show(`Deleted user`, 'success');
        this.props.onDelete(uid);
      })
      .catch(error => notify.show(error.message, 'error'))
      .finally(() => this.setState({ loading: false }));
  }

  handleDisableUser = (e) => {
    const { uid } = this.props.user;
    this.handleCloseMenu();
    this.setState({ loading: true });
    disableUser(uid)
      .then(user => {
        notify.show(`Disable user ${user.email}`, 'success');
        this.props.onUpdate(user);
      })
      .catch(error => notify.show(error.message, 'error'))
      .finally(() => this.setState({ loading: false }));
  }

  handleEnableUser = (e) => {
    const { uid } = this.props.user;
    this.handleCloseMenu();
    this.setState({ loading: true });
    enableUser(uid)
      .then(user => {
        notify.show(`Enable user ${user.email}`, 'success');
        this.props.onUpdate(user);
      })
      .catch(error => notify.show(error.message, 'error'))
      .finally(() => this.setState({ loading: false }));
  }

  openEditProfile = () => this.setState({ openEditProfile: true, openMenu: false })
  openResetPassword = () => this.setState({ openResetPassword: true, openMenu: false })

  handleEditProfile = (e) => {
    console.log('handleEditUser')
  }

  // handleResetPassword = (e) => {
  //   const { uid } = this.props.user;
  //   this.handleCloseMenu();
  //   this.setState({ loading: true });
  //   changePassword(uid, 'theagent')
  //     .then(user => {
  //       notify.show(`Reset user ${user.email}'s password to "theagent"`, 'success');
  //       this.props.onUpdate(user);
  //     })
  //     .catch(error => notify.show(error.message, 'error'))
  //     .finally(() => this.setState({ loading: false }));
  // }

  closeEditProfile = () => this.setState({ openEditProfile: false });
  closeResetPassword = () => this.setState({ openResetPassword: false });

  render() {
    const { user } = this.props;
    const { openMenu, loading, name, phoneNumber, company } = this.state;
    return (
      <TableRow key={user.uid} style={{ opacity: user.disabled ? 0.5 : 1 }}>
        <TableCell>{user.photoURL && <Avatar style={{ width: 25, height: 25 }} src={user.photoURL} alt={user.uid} />}</TableCell>
        <TableCell>{user.email}{user.disabled && <Chip label="Disabled" />}</TableCell>
        
        <TableCell>{company}{user.disabled && <Chip label="Disabled" />}</TableCell>
        <TableCell>{name}{user.disabled && <Chip label="Disabled" />}</TableCell>
        <TableCell>{phoneNumber}{user.disabled && <Chip label="Disabled" />}</TableCell>

        <TableCell>{moment(user.metadata.creationTime).format('MMM D, YYYY')}</TableCell>
        <TableCell>{user.metadata.lastSignInTime !== null ? moment(user.metadata.lastSignInTime).format('MMM D, YYYY') : 'Never'}</TableCell>
        <TableCell>
          {loading ?
            <CircularProgress size={36} />
            :
            <IconButton
              aria-label="More"
              aria-haspopup="true"
              buttonRef={node => this.anchorEl = node}
              onClick={e => this.handleClick(e, user.uid)}
            >
              <MoreVertIcon />
            </IconButton>
          }
          { user && //_.includes(user.role, ['mi-treasure.admin']) &&
            <React.Fragment>
              <Menu
                anchorEl={this.anchorEl}
                open={openMenu}
                onClose={this.handleCloseMenu}
              >
                <MenuItem onClick={this.openResetPassword}>Reset Password</MenuItem>
                {!user.disabled && <MenuItem onClick={this.handleDisableUser}>Disable User</MenuItem>}
                {user.disabled &&  <MenuItem onClick={this.handleEnableUser}>Enable User</MenuItem>}
                <MenuItem onClick={this.handleDeleteUser}>Delete User</MenuItem>
                <MenuItem onClick={this.openEditProfile}>Edit Profile</MenuItem>
              </Menu>
              <ResetPasswordDialog open={this.state.openResetPassword} onClose={this.closeResetPassword} user={user} />
              <EditProfileDialog open={this.state.openEditProfile} onClose={this.closeEditProfile} user={user} />
            </React.Fragment>
          }
        </TableCell>
      </TableRow>
    );
  }
}