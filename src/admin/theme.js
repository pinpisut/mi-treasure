import { createMuiTheme } from '@material-ui/core/styles';

export const primaryColor = '#EB6323';


const adminTheme = createMuiTheme({
  // Colors
  palette: {
    type: 'light',
    primary: {
      main: primaryColor,
    },
    secondary: {
      main: '#ff3b3b',
    }
  },

  // Typography
  typography: {
    fontFamily: ['SukhumvitSet-Medium'].join(','),
  },

  overrides: {
    MuiModal: {
      root: {
        alignItems: 'center',
        justifyContent: 'center'
      }
    },
    MuiPaper: {
      elevation2: {
        padding: 10,
        width: '30%'
      }
    }
  },
});

export default adminTheme;

