import _ from 'lodash';
import axios from 'axios';

export const fetchUsers = (user) => {
  console.log('fetchUsers');
  return axios({
    method: 'GET',
    url: `${process.env.REACT_APP_API}/admin/users`
  }).then(res => {
    if (_.get(res, 'data.status') !== 'success') {
      throw new Error(res.error);
    }
    // Success
    return _.get(res, 'data.data');
  }).catch(err => {
    console.log(err);
  });
}

export const createUser = (email, password, displayName, role) => {
  return axios({
    method: 'POST',
    url: `${process.env.REACT_APP_API}/admin/users`,
    data: {
      email,
      password,
      displayName,
      role,
    }
  }).then(res => {
    if (_.get(res, 'data.status') !== 'success') {
      throw new Error(res.error);
    }
    // Success
    return _.get(res, 'data.data');
  });
}

export const deleteUser = (uid) => {
  return axios({
    method: 'DELETE',
    url: `${process.env.REACT_APP_API}/admin/users/${uid}`,
  }).then(res => {
    if (_.get(res, 'data.status') !== 'success') {
      throw new Error(res.error);
    }
    // Success
    return _.get(res, 'data.data');
  });
}

export const changePassword = (uid, password) => {
  return axios({
    method: 'PUT',
    url: `${process.env.REACT_APP_API}/admin/users/${uid}`,
    data: {
      password,
    }
  }).then(res => {
    if (_.get(res, 'data.status') !== 'success') {
      throw new Error(res.error);
    }
    // Success
    return _.get(res, 'data.data');
  });
}

export const disableUser = (uid, password) => {
  return axios({
    method: 'PUT',
    url: `${process.env.REACT_APP_API}/admin/users/${uid}`,
    data: {
      disabled: true,
    }
  }).then(res => {
    if (_.get(res, 'data.status') !== 'success') {
      throw new Error(res.error);
    }
    // Success
    return _.get(res, 'data.data');
  });
}

export const enableUser = (uid, password) => {
  return axios({
    method: 'PUT',
    url: `${process.env.REACT_APP_API}/admin/users/${uid}`,
    data: {
      disabled: false,
    }
  }).then(res => {
    if (_.get(res, 'data.status') !== 'success') {
      throw new Error(res.error);
    }
    // Success
    return _.get(res, 'data.data');
  });
}
