import React from 'react';
import _ from 'lodash';
import { Route, NavLink, Link } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import styled, { css } from 'styled-components';

import { MuiThemeProvider } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import MenuList from '@material-ui/core/MenuList';
import MenuItem from '@material-ui/core/MenuItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Drawer from '@material-ui/core/Drawer';

import IconProject from '@material-ui/icons/Domain';
import IconPeople from '@material-ui/icons/People';
import IconFacility from '@material-ui/icons/PinDrop';

import adminTheme from './theme';
import ProjectList from './screens/ProjectList';
import ProjectDetail from './screens/ProjectDetail';

import PeopleList from './screens/PeopleList';

import Facilities from './screens/FacilitytList';
import FacilityDetail from './screens/FacilityDetail';

import { primaryColor } from './theme';
import Logo from './components/LogoTheAgent';

import { signOut } from '../engine/auth';

const Wrapper = styled.div`
  width: 100vw;
  height: 100vh;
  background: #fafafa;
  box-sizing: border-box;
  display: flex;
`;

const BrandHeader = styled.div`
  background: #ddd;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  padding: 40px 20px;

  svg {
    width: 80%;
  }
`;

const Sidebar = styled.div`
  flex: 0 0 240px;
  display: flex;
  border-right: 1px solid #ececec;
  flex-direction: column;
  justify-content: flex-start;

  .sidebar-main {
    flex-grow: 1;
  }
`;

const Content = styled.div`
  flex-grow: 1;
  padding: 40px;
  box-sizing: border-box;
  background: white;
  overflow: scroll;
`;


const StyledMenuItem = styled(MenuItem)`
  &&& {
    color: black;
    span { color: inherit; }
    svg {
      fill: black;
    }
    &:hover {
      color: ${primaryColor};
      svg {
        fill: ${primaryColor};
      }
    }

    &.active {
      background: ${primaryColor};
      color: white;
      svg {
        fill: white;
      }
    }

    ${props => props.selected && css`
      background: ${primaryColor};
      color: white;
      svg {
        fill: white;
      }
    `}
  }
`;


export default ({ match, user }) => (
  <MuiThemeProvider theme={adminTheme}>
    <Helmet>
      <title>Admin | MI Treasure</title>
    </Helmet>
    <Wrapper>
      <Sidebar>
        <BrandHeader>
          <Logo />
          <hr />
          <Typography variant="headline">MI TREASURE</Typography>
        </BrandHeader>
        <div className="sidebar-main">
          <MenuList>
            <StyledMenuItem button component={NavLink} to={`${match.url}/projects`}>
              <ListItemIcon>
                <IconProject />
              </ListItemIcon>
              <ListItemText>Projects</ListItemText>
            </StyledMenuItem>
            <StyledMenuItem button component={NavLink} to={`${match.url}/people`}>
              <ListItemIcon>
                <IconPeople />
              </ListItemIcon>
              <ListItemText>People</ListItemText>
            </StyledMenuItem>
            <StyledMenuItem button component={NavLink} to={`${match.url}/facilities`}>
              <ListItemIcon>
                <IconFacility />
              </ListItemIcon>
              <ListItemText>Facilities</ListItemText>
            </StyledMenuItem>
          </MenuList>
        </div>
        <div>
          <MenuList>
            <StyledMenuItem button onClick={signOut}>
              <ListItemText inset>Logout</ListItemText>
            </StyledMenuItem>
          </MenuList>
        </div>
      </Sidebar>
      <Content>
        <Route path={`${match.url}/projects`} component={ProjectList} />
        <Route path={`${match.url}/projects/:projectId`} children={({ match, ...rest }) => {
          const projectId = _.get(match, 'params.projectId');
          return (
            <Drawer anchor="right" open={projectId !== undefined} onClose={() => rest.history.goBack()}>
              <ProjectDetail {...rest} projectId={projectId} />
            </Drawer>
          );
        }} />
        <Route path={`${match.url}/people`} render={routerProps => <PeopleList {...routerProps} user={user} />} />
        <Route path={`${match.url}/facilities`} render={routerProps => <Facilities {...routerProps} user={user} />} />
        <Route path={`${match.url}/facility/:facilityId`} render={({ match, ...rest }) => {
          const facilityId = _.get(match, 'params.facilityId');
          if(!facilityId) {
            return null
          }
          return (
            <FacilityDetail {...rest} facilityId={facilityId} />
          );
        }} />
        
      </Content>
    </Wrapper>
  </MuiThemeProvider>
)