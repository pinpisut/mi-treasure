import React from 'react';
import { Helmet } from 'react-helmet';
import _ from 'lodash';
import styled from 'styled-components';
import Button from '@material-ui/core/Button';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import AddIcon from '@material-ui/icons/AddCircle';
import Modal from '@material-ui/core/Modal';

import PageHeader from '../components/PageHeader';
import FacilityTableRow from '../components/FacilityTableRow';
import * as Color from '../../components/Color';

import { P } from '../../components/Typography';
import { db } from '../../engine';

const BlockContent = styled.div`
  min-height: 100%;
`;

const AddFacilityButton = props => {
  return (
    <Button variant="extendedFab" color="primary" {...props}>
      <AddIcon style={{ color: 'white', marginRight: 8 }}/> Add Facility
    </Button>
  );
}

export default class extends React.PureComponent {
  state = {
    facilities: [],
    openModel: false
  }

  componentDidMount() {
    db.collection('facilities')
      .orderBy('title')
      .limit(50)
      .get()
      .then(querySnapshot => {
        // console.log('querySnapshot', querySnapshot.docs)
        this.setState({
          facilities: _.map(querySnapshot.docs, doc => doc.id),
          lastFacility: querySnapshot.docs[querySnapshot.docs.length - 1],
        })
      })
  }

  loadMore = () => {
    db.collection('facilities')
      .orderBy('title')
      .limit(50)
      .startAfter(this.state.lastFacility)
      .get()
      .then(querySnapshot => {
        // console.log('querySnapshot', querySnapshot.docs)
        this.setState({
          facilities: [
            ...this.state.facilities,
            ..._.map(querySnapshot.docs, doc => doc.id),
          ],
          lastFacility: querySnapshot.docs[querySnapshot.docs.length - 1],
        });
      });
  }

  handleSelectFacility = (facilityId) => {
    const { history } = this.props;
    history.push(`/admin/facility/${facilityId}`);
  }

  handleOpenModal = () => {
    const { history } = this.props;
    history.push('/admin/facility/0');
  }

  handleCloseModal = () => {
    this.setState({ openModel: false });
  }

  handleSave = (facilityId) => {
    const { history } = this.props;
    history.push(`/admin/facilities/${facilityId}`);
    this.handleCloseModal();
  }

  render() {
    const { facilities } = this.state;
    return (
      <BlockContent>
        <Helmet>
          <title>Facilities | MI Treasure</title>
        </Helmet>
        {/*
        <PageHeader>
        <Typography variant="headline">Facilities</Typography>
        <AddFacilityButton
          style={{ display: 'flex', alignItems: 'center', backgroundColor: Color.primary }}
          onClick={this.handleOpenModal}
        />
        </PageHeader>
        */}
        <PageHeader
          title="Facilities"
          renderActionButtons={() => (
            <AddFacilityButton
              style={{ backgroundColor: Color.primary }}
              onClick={this.handleOpenModal}
            />
          )}
        />
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>ID</TableCell>
              <TableCell>Title</TableCell>
              <TableCell>TYPE</TableCell>
              <TableCell>LAT</TableCell>
              <TableCell>LON</TableCell>
            </TableRow>
          </TableHead>
        <TableBody>
          {facilities.map(facilityId =>
            <FacilityTableRow
              key={facilityId}
              id={facilityId}
              onClick={e => this.handleSelectFacility(facilityId)}
            />
          )}
        </TableBody>
      </Table>
      <Button variant="raised" color="primary" onClick={this.loadMore}>Load More</Button>
      <Modal
        open={this.state.openModel}
        onClose={this.handleCloseModal}
      >
      </Modal>
      </BlockContent>
    );
  }
}