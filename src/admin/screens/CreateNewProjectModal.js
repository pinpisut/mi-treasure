import React from 'react';
import styled from 'styled-components';

import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';

import * as Color from '../../components/Color';
import { P, H6 } from '../../components/Typography';



const PaperContent = styled.div`
  padding: 0px 40px;
  box-sizing: border-box;
`;

const ActionBar = styled.div`
  box-sizing: border-box;
  margin: 20px 0px;
  width: 100%;
  background: transparent;
  display: flex;
  justify-content: flex-end; 
`;

const InputLabel = styled.div`
  padding: 20px 0px;
  border-bottom: 2px solid ${Color.primary};
`;


export default class extends React.PureComponent {

  static defaultProps = {
    CloseModal: () => null,
    OnValue: () => null
  }

  state = {

  }

  handleCloseModal = () => {
    this.props.CloseModal(false)

  }

  handleSave = () => {
    const id = document.getElementById('input-prjectId').value;
    const title = document.getElementById('input-projectTitle').value;
    const projectId = `${id}.${title}`;
    this.props.OnValue(projectId);
  }

  render() {
    return (
      <Paper style={{ width: '35%', position: 'relative', padding: '10', boxSizing: 'border-box' }}>
        <PaperContent>
        <InputLabel><H6 style={{ margin: 0 }}>Create a new Project</H6></InputLabel>
        <TextField
          id="input-prjectId"
          label="Project Id"
          placeholder="Project Id"
          margin="normal"
          fullWidth
        />
        <TextField
          id="input-projectTitle"
          label="Project Title"
          placeholder="Project Title"
          margin="normal"
          fullWidth
        />
        <ActionBar>
          <Button onClick={this.handleCloseModal}><P>cancel</P></Button>
          <Button color="primary" onClick={this.handleSave}><P>save</P></Button>
        </ActionBar>
        </PaperContent>
      </Paper>
    );
  }
}