import React from 'react';
import styled from 'styled-components';
import _ from 'lodash';
import { notify } from '../../components/Notification';

import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Divider from '@material-ui/core/Divider';
import Input from '@material-ui/core/Input';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

import Button from '../../components/Button';

import { db } from '../../engine';

import { updateProject, updateProjectHistory } from '../api/project';

const DRAWER_WIDTH = 600;

const PaperContent = styled.div`
  padding: 40px;
  box-sizing: border-box;
`;

const Heading = styled.div`
  color: #5a5a5a;
  font-weight: bold;
  font-size: 0.875rem;
`;

const SeconaryHeading = styled.div`
  color: #787878;
  font-size: 0.875rem;
`;

const Section = styled.section`
  margin: 25px 0;
`;

const ActionBar = styled.div`
  box-sizing: border-box;
  padding: 20px;
  position: fixed;
  right: 0;
  bottom: 0;
  width: ${DRAWER_WIDTH}px;
  background: #fafafa;
  border-top: 1px solid #ececec;
  display: flex;
  justify-content: flex-end;
`;


const mapDataToFieldValues = (data) => {
  return {
    title: {
      label: 'Title',
      value: _.get(data, 'title.en', _.get(data, 'title', '')),
    },
    titleTH: {
      label: 'Title (ไทย)',
      value: _.get(data, 'title.th', ''),
    },
    yearOpen: {
      label: 'Year Open',
      value: _.get(data, 'yearOpen', ''),
    },
    yearComplete: {
      label: 'Year Complete',
      value: _.get(data, 'yearComplete', ''),
    },
    startingPrice: {
      label: 'Starting Price',
      value: _.get(data, 'startingPrice', ''),
    },
    theAgentProjectId: {
      label: 'ProjectID',
      value: _.get(data, 'integrations.TheAgent.ProjectID', ''),
    },
    theAgentProjectCode: {
      label: 'ProjectCode',
      value: _.get(data, 'integrations.TheAgent.ProjectCode', ''),
    },
    theAgentProjectLink: {
      label: 'Link',
      value: _.get(data, 'integrations.TheAgent.ProjectLink', ''),
    },
  }
}

class PriceHistoryRow extends React.PureComponent {
  static getDerivedStateFromProps (nextProps, prevState) {
    if (nextProps.salePrice !== prevState.salePrice || nextProps.rentPrice !== prevState.rentPrice || nextProps.remark !== prevState.remark) {
      return {
        salePrice: nextProps.salePrice,
        rentPrice: nextProps.rentPrice,
        remark: nextProps.remark,
      }
    }
    return null;
  }

  state = {
    editMode: false,
    salePrice: this.props.salePrice,
    rentPrice: this.props.rentPrice,
    remark: this.props.remark,
  }

  enterEditMode = (e) => {
    this.setState({ editMode: true });
  }

  save = (e) => {
    const { time, source } = this.props;
    const newSalePrice = _.toNumber(this.salePriceInput.value);
    const newRentPrice = _.toNumber(this.rentPriceInput.value);
    const newRemark = this.remarkInput.value;
    this.setState({ editMode: false });
    this.props.onSave(time, source, newSalePrice, newRentPrice, newRemark);
  }
  render() {
    const { source } = this.props;
    const { editMode, salePrice, rentPrice, remark } = this.state;
    return (
      <TableRow key={source}>
        <TableCell padding="dense">
          {!editMode ?
            <Button size="small" color="primary" onClick={this.enterEditMode}>Edit</Button>
            :
            <Button size="small" color="primary" variant="raised" onClick={this.save}>Save</Button>
          }
        </TableCell>
        <TableCell padding="dense">{source}</TableCell>
        <TableCell padding="dense">{!editMode ? salePrice :
          <Input defaultValue={salePrice} style={{ fontSize: '0.8rem', width: 60 }} inputRef={c => this.salePriceInput = c} />}</TableCell>
        <TableCell padding="dense">{!editMode ? rentPrice :
          <Input defaultValue={rentPrice} style={{ fontSize: '0.8rem', width: 60 }} inputRef={c => this.rentPriceInput = c} />}</TableCell>
        <TableCell padding="dense">{!editMode ? remark :
          <Input defaultValue={remark} style={{ fontSize: '0.8rem', width: 60 }} inputRef={c => this.remarkInput = c} />}</TableCell>
      </TableRow>
    );
  }
} 

const mapFieldValuesToData = (fieldValues) => {
  return {
    title: {
      en: fieldValues.title,
      th: fieldValues.titleTH,
    },
    yearOpen: fieldValues.yearOpen,
    yearComplete: fieldValues.yearComplete,
    startingPrice: fieldValues.startingPrice,
    integrations: {
      TheAgent: {
        ProjectID: fieldValues.theAgentProjectId,
        ProjectCode: fieldValues.theAgentProjectCode,
        ProjectLink: fieldValues.theAgentProjectLink,
      }
    }
    
  }
}

export default class extends React.PureComponent {

  static defaultProps = {
    id: 'teemp',
  }

  state = {
    fields: mapDataToFieldValues(),
  }

  loadProject = (projectId) => {
    if (projectId) {
      this.unsubscribeProject = db.collection('projects')
        .doc(projectId)
        .onSnapshot(doc => {
          // console.log('querySnapshot', querySnapshot.docs)
          this.setState({
            fields: mapDataToFieldValues(doc.data()),
          });
        });

      this.unsubscribeProjectHistory = db.collection('projects')
        .doc(projectId)
        .collection('history')
        .orderBy('time', 'desc')
        .onSnapshot(querySnapshot => {
          const history = querySnapshot.docs;
          this.setState({
            priceHistory: _.map(history, h => h.data()),
          });
        })
    }
  }

  componentDidMount() {
    this.loadProject(this.props.projectId);
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.projectId !== prevProps.projectId) {
      this.loadProject(this.props.projectId);
    }
  }

  componentWillUnmount() {
    this.unsubscribeProject();
    this.unsubscribeProjectHistory();
  }

  save = (e) => {
    const { projectId, history } = this.props;
    e.preventDefault();
    const fieldValues = _.mapValues(this.state.fields, field => field.value);
    const data = mapFieldValuesToData(fieldValues);
    
    // db.collection('projects').doc(projectId).set(data, { merge: true });
    updateProject(projectId, data);

    notify.show('Project Saved', 'success');

    history.push(`/admin/projects`);
  }

  handleSavePriceHistory = (time, source, newSalePrice, newRentPrice, newRemark) => {
    console.log('save', time, source, newSalePrice, newRentPrice);
    const { projectId } = this.props;
    updateProjectHistory(projectId, time, source, {
      salePrice: newSalePrice,
      rentPrice: newRentPrice,
      remark: newRemark,
    });
  }


  handleInputChange = (name, value) => {
    this.setState({
      fields: {
        ...this.state.fields,
        [name]: {
          ...this.state.fields[name],
          value,
        }
      }
    })
  }


  renderField = (fieldName) => {
    const field = _.get(this.state, `fields.${fieldName}`);
    const { label, value } = field;
    return (
      <div style={{ display: 'flex', padding: '6px 0', justifyContent: 'space-between', alignItems: 'center' }}>
        <div style={{ flexBasis: 200 }}><Typography variant="caption">{label}</Typography></div>
        <div style={{ flex: 1}}>
          <Input
            fullWidth value={value} style={{ boxShadow: 'none' }}
            onChange={e => this.handleInputChange(fieldName, e.target.value)}
          />
        </div>
      </div>
    );
  }

  render() {
    const { projectId, history } = this.props;
    const { fields, priceHistory } = this.state;
    return (
      <div style={{ height: '100vh', position: 'relative', overflow: 'scroll' }}>
        <Paper elevation={0} style={{ width: DRAWER_WIDTH, padding: '40px 0', boxSizing: 'border-box' }}>
          <PaperContent>
            <Typography variant="display1">{_.get(fields, 'title.value', '')}</Typography>
            <Section>
              <Typography variant="subheading">Info</Typography>
              <div>
                {this.renderField('title')}
                {this.renderField('titleTH')}
                {this.renderField('yearOpen')}
                {this.renderField('yearComplete')}
                {this.renderField('startingPrice')}
              </div>
            </Section>
            <Divider />
            <Section>
              <Typography variant="subheading">The Agent</Typography>
              <div>
                {this.renderField('theAgentProjectId')}
                {this.renderField('theAgentProjectCode')}
                {this.renderField('theAgentProjectLink')}
              </div>
            </Section>
            <Divider />
            <Section>
              <Typography variant="subheading">History</Typography>
              {_.map(priceHistory, time => (
                <ExpansionPanel>
                  <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                    <div style={{ display: 'flex', flex: 1, justifyContent: 'space-between' }}>
                      <Heading>{time.time}</Heading>
                      <SeconaryHeading>{time.salePrice} <span>/sq.m.</span></SeconaryHeading>
                      <SeconaryHeading>{time.rentPrice} /sq.m.</SeconaryHeading>
                    </div>
                  </ExpansionPanelSummary>
                  <ExpansionPanelDetails style={{ padding: 0 }}>
                    <Table style={{ maxWidth: '100%' }}>
                      <TableHead>
                        <TableRow>
                          <TableCell style={{ width: 40 }}></TableCell>
                          <TableCell>Source</TableCell>
                          <TableCell>Sale</TableCell>
                          <TableCell>Rent</TableCell>
                          <TableCell>Remark</TableCell>
                        </TableRow>
                      </TableHead>
                      <TableBody>
                        {_.map(time.sources, source =>
                          <PriceHistoryRow
                            time={time.time}
                            {...source}
                            onSave={this.handleSavePriceHistory}
                          />
                        )}
                      </TableBody>
                    </Table>
                  </ExpansionPanelDetails>
                </ExpansionPanel>
              ))}
            </Section>
          </PaperContent>
        </Paper>
        <ActionBar>
          <Button onClick={history.goBack}>Cancel</Button>
          <Button color="primary" onClick={this.save}>Save</Button>
        </ActionBar>
      </div>
    );
  }
}