import React from 'react';
import { Helmet } from 'react-helmet';
import _ from 'lodash';
import styled from 'styled-components';
import Button from '@material-ui/core/Button';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import AddIcon from '@material-ui/icons/AddCircle';
import Modal from '@material-ui/core/Modal';
import Toolbar from '@material-ui/core/Toolbar';


import ProjectTableRow from '../components/ProjectTableRow';
import PageHeader from '../components/PageHeader';

import * as Color from '../../components/Color';
import { P } from '../../components/Typography';
import { db } from '../../engine';

import NewProject from './CreateNewProjectModal';

const AddProjectButton = props => {
  return (
    <Button variant="extendedFab" color="primary" {...props}>
      <AddIcon style={{ color: 'white', marginRight: 8 }} /> Add Project
    </Button>
  );
}

export default class extends React.PureComponent {
  state = {
    projects: [],
    openModel: false
  }

  componentDidMount() {
    db.collection('projects')
      .orderBy('title')
      .limit(50)
      .get()
      .then(querySnapshot => {
        // console.log('querySnapshot', querySnapshot.docs)
        this.setState({
          projects: _.map(querySnapshot.docs, doc => doc.id),
          lastProject: querySnapshot.docs[querySnapshot.docs.length - 1],
        })
      })
  }

  loadMore = () => {
    db.collection('projects')
      .orderBy('title')
      .limit(50)
      .startAfter(this.state.lastProject)
      .get()
      .then(querySnapshot => {
        // console.log('querySnapshot', querySnapshot.docs)
        this.setState({
          projects: [
            ...this.state.projects,
            ..._.map(querySnapshot.docs, doc => doc.id),
          ],
          lastProject: querySnapshot.docs[querySnapshot.docs.length - 1],
        });
      });
  }

  handleSelectProject = (projectId) => {
    const { history } = this.props;
    history.push(`/admin/projects/${projectId}`);
  }

  handleOpenModal = () => {
    this.setState({ openModel: true });
  }

  handleCloseModal = () => {
    this.setState({ openModel: false });
  }

  handleSave = (projectId) => {
    const { history } = this.props;
    history.push(`/admin/projects/${projectId}`);
    this.handleCloseModal();
  }

  render() {
    const { projects } = this.state;
    return (
      <div>
        <Helmet>
          <title>Projects | MI Treasure</title>
        </Helmet>
        <PageHeader
          title="Projects"
          renderActionButtons={() => (
            <AddProjectButton
              style={{ display: 'flex', alignItems: 'center', backgroundColor: Color.primary }}
              onClick={this.handleOpenModal}
            />
          )}
        />
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>ID</TableCell>
              <TableCell>Title</TableCell>
              <TableCell>TheAgent ID</TableCell>
              <TableCell>Sale</TableCell>
              <TableCell>Rent</TableCell>
              <TableCell>TheAgent</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {projects.map((projectId, index) =>
              <ProjectTableRow
                key={`${projectId}-${index}`}
                id={projectId}
                onClick={e => this.handleSelectProject(projectId)}
              />
            )}
          </TableBody>
        </Table>
        <Toolbar>
          <Button variant="raised" color="primary" onClick={this.loadMore}>Load More</Button>
        </Toolbar>
        <Modal
          open={this.state.openModel}
          onClose={this.handleCloseModal}
        >
          <NewProject OnValue={projectId => this.handleSave(projectId)} CloseModal={this.handleCloseModal}/>
        </Modal>
      </div>
    );
  }
}