import React from 'react';
import _ from 'lodash';
import styled from 'styled-components';

import Typography from '@material-ui/core/Typography';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

import { notify } from '../../components/Notification';
import { fetchUsers } from '../api/user';
import PageHeader from '../components/PageHeader';
import UserTableRow from '../components/UserTableRow';
import NewUserButton from '../components/NewUserButton';

const filterByRoles = (users, role) => {
  return _.filter(users, user => _.includes(_.get(user, 'customClaims.roles', ''), role));
}

export default class extends React.PureComponent {

  static defaultProps = {
    user: null,
  }

  state = {
    anchorEl: null,
    uid: null,
    users: [],
    role: 'mi-treasure.admin',
    openNewUserModal: false,
  }

  componentDidMount() {
    this.loadData();
  }

  loadData = () => {
    fetchUsers()
      .then(users => {
        this.setState({ users })
      })
      .catch(error => {
        notify.show(error.message, 'error', 0);
      });
  }

  handleChangeTab = (event, role) => {
    this.setState({ role });
  }

  handleCreateSuccess = user => {
    // Optimistic Update
    this.setState({
      users: [...this.state.users, user],
    })
    notify.show(`Created user ${user.email}`, 'success');
    
    // Reload Data from Firebase
    this.loadData();
  }

  onUpdateUser = updatedUser => {
    // Optimistic Update
    this.setState({
      users: _.map(this.state.users, user => {
        if (user.uid === updatedUser.uid) return Object.assign({}, user, updatedUser);
        return user;
      })
    });

    // Reload Data from Firebase
    this.loadData();
  }

  onDeleteUser = uid => {
    this.setState({
      users: _.filter(this.state.users, user => user.uid !== uid)
    });
  }

  render() {
    const { users, role } = this.state;
    const visibleUsers = filterByRoles(users, role);
    return (
      <div>
        <PageHeader
          title="People"
          renderActionButtons={() => (
            <NewUserButton
              onSuccess={this.handleCreateSuccess}
              onError={error => notify.show(error.message, 'error', 0)}
            />
          )}
        >
          <Tabs value={role} onChange={this.handleChangeTab}>
            <Tab value="mi-treasure.admin" label="Admin" />
            <Tab value="mi-treasure.sale" label="Sale" />
            <Tab value="mi-treasure.alliance" label="Alliance" />
          </Tabs>
        </PageHeader>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell></TableCell>
              <TableCell>Email</TableCell>
              <TableCell>Company</TableCell>
              <TableCell>Name</TableCell>
              <TableCell>Phone</TableCell>
              <TableCell>Created</TableCell>
              <TableCell>SignedIn</TableCell>
              <TableCell></TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {visibleUsers.map(user => { 
              return (
              <UserTableRow key={user.uid} user={user} onUpdate={this.onUpdateUser} onDelete={this.onDeleteUser} />)}
            )}
          </TableBody>
        </Table>
      </div>
    );
  }
}