import React, { Component } from 'react';
import styled from 'styled-components';
import axios from 'axios';
import _ from 'lodash';
import { BrowserRouter as Router, Switch, Route, Redirect, Link } from 'react-router-dom';
import { createStore, applyMiddleware } from 'redux';
import logger from 'redux-logger';
import thunk from 'redux-thunk';
import { Provider } from 'react-redux';
import { MuiThemeProvider } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';

import Notifications from './components/Notification';
import Header from './components/Header';

import NoMatch from './screens/404';
import StyleGuide from './screens/Styleguide';
import Login from './screens/Login';
import MapScreen from './screens/MapScreen';
import CompareScreen from './screens/Compare';
import HomeScreen from './screens/HomeScreen';
import DesignSetting from './components/DesignSetting';

// Admin
import Admin from './admin';

import { auth, db } from './engine';

import './App.css';

import rootReducer from './reducers';
import { fetchPointOfInterests, fetchFacilities } from './api';
import theme from './theme';

const AuthenticatedContext = React.createContext(null);

const FullscreenOverlay = styled.div`
  display: flex;
  flex-direction: column;
  width: 100vw;
  height: 100vh;
  justify-content: center;
  align-items: center;
`;

let store = null;
if (process.env.NODE_ENV !== 'production') {
  const {whyDidYouUpdate} = require('why-did-you-update');
  whyDidYouUpdate(React);
  store = createStore(rootReducer, applyMiddleware(thunk, logger));
} else {
  store = createStore(rootReducer, applyMiddleware(thunk));
}


class PrivateRoute extends React.PureComponent {
  render() {
    return (
      <AuthenticatedContext.Consumer>
        {({ user, userLoaded }) => {
          // Hasn't load user.
          if (!userLoaded) {
            return (
              <FullscreenOverlay>
                <CircularProgress color="primary" />
                <div>Checking User</div>
              </FullscreenOverlay>
            );
          }
          
          // Hasn't login.
          if (!user) return (<Redirect to={{pathname: '/login', state: { from: this.props.location }}} />);

          // Everything ok
          return <Route {...this.props} />
        }}
      </AuthenticatedContext.Consumer>
    );
  }
}

class AdminRoute extends React.PureComponent {
  render() {
    return (
      <AuthenticatedContext.Consumer>
        {({ user, userLoaded }) => {
          console.log('AdminRoute', user);
          // Hasn't load user.
          if (!userLoaded) {
            return (
              <FullscreenOverlay>
                <CircularProgress color="primary" />
                <div>Checking User</div>
              </FullscreenOverlay>
            );
          }
          
          // Hasn't login.
          if (!user) return (<Redirect to={{pathname: '/login', state: { from: this.props.location }}} />);

          // Not Admin
          if (!_.includes(_.get(user, 'roles', 'no-roles'), 'mi-treasure.admin')) return (<div style={{ display: 'flex', width: '100vw', height: '100vh', justifyContent: 'center', alignItems: 'center' }}>You are not admin, please contact K.Dop</div>);

          // Everything ok
          return <Route {...this.props} />
        }}
      </AuthenticatedContext.Consumer>
    );
  }
}

class App extends Component {
  state = {
    loaded: false,
    user: null,
    userLoaded: false,
  }
  componentDidMount() {
    // console.log('iam', iamEngine);
    auth.onAuthStateChanged(user => {
      if (user !== null) {
        axios.defaults.headers.common['Authorization'] = _.get(user, 'stsTokenManager.accessToken');
      }
      this.setState({
        user,
        userLoaded: true,
      });
    });

    fetchFacilities()
      .then(facilities => {
        store.dispatch({
          type: 'ENTITIES/FACILITIES/RECEIVED',
          items: facilities,
        })
      }).catch(err => { 
          console.log('err',  err) 
        })

    fetchPointOfInterests()
      .then(pointOfInterests => {
        store.dispatch({
          type: 'ENTITIES/POINT_OF_INTERESTS/RECEIVED',
          items: pointOfInterests,
        });

        const projects = _.filter(pointOfInterests, poi => poi.type === 'PROJECT');
        store.dispatch({
          type: 'ENTITIES/PROJECTS/RECEIVED',
          items: projects,
        });

        this.setState({ loaded: true })
      });
  }

  render() {
    const { loaded, userLoaded, user } = this.state;
    
    if (!loaded) return (
      <FullscreenOverlay>
        <CircularProgress color="secondary" />
        <div>Loading Data</div>
      </FullscreenOverlay>);
    return (
      <AuthenticatedContext.Provider value={{ user, userLoaded }}>
        <Provider store={store}>
          <MuiThemeProvider theme={theme}>
            <Notifications options={{ zIndex: 5000 }} />
            <Router>
              <div className="App">
                <Route
                  path="/"
                  children={({ location, ...rest }) => (
                    <div>
                      {!/^\/admin/.test(location.pathname) && user &&
                        <Header auth={auth} user={user} />
                      }
                    </div>
                  )}
                />
                <Switch>
                  <Route exact path="/" render={() => <Redirect to="/map/BTS.S3/projects" />}/>
                  <Route exact path="/styleguide" component={StyleGuide} />
                  <Route exact path="/home" component={HomeScreen} />
                  <Route exact path="/login" render={routerProps => <Login {...routerProps} auth={auth} />} />
                  <Route exact path="/map" render={() => <Redirect to="/map/BTS.S3/projects" />} />
                  <PrivateRoute
                    path="/map/:poi?/:page?"
                    component={MapScreen}
                  />
                  <PrivateRoute
                    path="/compare/:poi?/:ids"
                    component={CompareScreen}
                  />
                  <AdminRoute
                    path="/admin"
                    component={Admin}
                  />
                  <Route component={NoMatch} />
                </Switch>
                {/* <Route path="/map" component={DesignSetting} /> */}
              </div>
            </Router>
          </MuiThemeProvider>
        </Provider>
      </AuthenticatedContext.Provider>
    );
  }
}

export default App;
