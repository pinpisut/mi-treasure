import _ from 'lodash';

export const cleanFacilities = data => {
  const allCateogries = _.map(data, item => {
    if (item.category) return item.category;
  });

  const cateogries = _.uniq(allCateogries);
  const groupCateogries = _.filter(cateogries, category => {
    if (category !== 'extra Group (ไม่มีผลในการคำนวน แสดงผลในแผนที่เฉยๆ)' && category !== undefined && category !== 'future-project') return category;
  });

  return {
    groupCateogries
  };
};
