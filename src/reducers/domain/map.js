import _ from 'lodash';

const DEFAULT_RADIUS = 600;

const initialState = {
  query: {
    poi: {
      value: '',
    },
    radius: DEFAULT_RADIUS,
    budget: {
      value: 'all',
      options: [
        // { value: 'all', min: 0, max: 999000000, label: 'ช่วงราคาทั้งหมด' },
        { value: '0-0.1', min: 0, max: 100000, label: 'น้อยกว่า 100k/sq.m.' },
        { value: '0.1-0.3', min: 100000, max: 140000, label: '100k-140k/sq.m.' },
        { value: '0.14-0.18', min: 140000, max: 180000, label: '140k-180k/sq.m.' },
        { value: '0.18-0.22', min: 180000, max: 220000, label: '180k-220k/sq.m.' },
        { value: '0.22-0.26', min: 220000, max: 260000, label: '220k-260k/sq.m.' },
        { value: '0.26-0.30', min: 260000, max: 300000, label: '260k-300k/sq.m.' },
        { value: '0.3+', min: 1000000, max: 999000000, label: '> 300k/sq.m. ขึ้นไป' },
      ],
    },
    yearComplete: {
      value: 'all',
      options: [
        { label: 'ปีที่สร้างเสร็จ', min: '1900', max: '9999', value: 'all' },
        { label: 'incomplete', min: '2018', max: '9999', value: '2018' },
        { label: '1-5 years', min: '2013', max: '2017', value: '2013-2017' },
        { label: '6-10 years', min: '2008', max: '2012', value: '2008-2012' },
        { label: 'more than 10 years', min: '1900', max: '2007', value: '0-2007' },
      ],
    },
  },
  viewOptionList: [
    'yearComplete',
    'salePrice',
    'rentPrice',
    'capitalGain',
    'rentalYield'
  ],
  result: [],
  selectedProject: [],
  // ID ของ Projects ที่แสดงผลแผนที่
  initVisibleOnMapProjects: [],
  visibleOnMapProjects: [],

}

export default (state = initialState, action) => {
  switch (action.type) {
    case 'MAP/QUERY/CHANGE':
      return {
        ...state,
        query: {
          ...state.query,
          [action.name]: {
            ...state.query[action.name],
            value: action.value,
          }
        },
      }
    case 'MAP/QUERY/CHANGE/RADIUS':
      return {
        ...state,
        query: {
          ...state.query,
          radius: action.radius,
        }
      }
    // Search Parameter มีการเปลี่ยนแปลง ค้นหา Projects ใหม่
    case 'MAP/RESULT/UPDATE': {
      const projectIds = _.map(action.result, p => p.id);
      return {
        ...state,
        result: projectIds,
        initVisibleOnMapProjects: projectIds,
        // visibleOnMapProjects: projectIds
      }
    }
    case 'MAP/RESULT/STATIONS': {
      return {
        ...state,
        stationOnMap: action.result,
      }
    }
    case 'COMPARE/SELECT/ALL':
      return initialState;
    case 'PROJECT/SELECT':
      return {
        ...state,
        select: 'some',
        selectedProject: [...state.selectedProject, action.id],
      }
    case 'PROJECT/DESELECT':
      return {
        ...state,
        select: 'some',
        selectedProject: _.filter(state.selectedProject, id => id !== action.id),
      }
    
    case 'DOMAIN/MAP/PROJECT/SHOWON/MAP':
      return {
        ...state,
        visibleOnMapProjects: [...state.visibleOnMapProjects, action.id],
      }
    case 'DOMAIN/MAP/PROJECT/HIDEON/MAP':
      return {
        ...state,
        visibleOnMapProjects: _.filter(state.visibleOnMapProjects, id => id !== action.id),
      }
    case 'DOMAIN/MAP/PROJECT/SHOWON/MAP/ALL':
      return {
        ...state,
        visibleOnMapProjects: [...action.ids],
      }
    case 'DOMAIN/MAP/PROJECT/HIDEON/MAP/ALL':
      return {
        ...state,
        visibleOnMapProjects: [],
      }
    
    case 'DOMAIN/MAP/VIEWOPTION/CHANGE':
      return {
        ...state,
        viewOptionList: action.values,
      }
    default: return state;
  }
}