const initialState = {
  layout: 'B',
  legend: 'B',
  overviewPosition: 'B',
  projectCard: 'A'
}

export default (state = initialState, action) => {
  switch (action.type) {
    case 'DESIGN/CHANGE': {
      return {
        ...state,
        [action.key]: action.value,
      }
    }
    default: return state;
  }
}