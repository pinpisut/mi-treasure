import { combineReducers } from 'redux';

import pointOfInterests from './entities/pointOfInterests';
import projects from './entities/projects';
import facilities from './entities/facilities';

import design from './domain/design';
import map from './domain/map';
import compare from './domain/compare';

export default combineReducers({
  entities: combineReducers({
    pointOfInterests,
    projects,
  }),
  domain: combineReducers({
    design,
    map,
    compare,
  }),
  facilities,
});