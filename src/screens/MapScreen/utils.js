import _ from 'lodash';
import moment from 'moment';
import * as Color from '../../components/Color';
import { getDistance } from '../../utils/map-utils';

const yellowPin = require('./img/yellow-pin.png');
const greenPin = require('./img/green-pin.png');
const orangePin = require('./img/orange-pin.png');
const bluePin = require('./img/blue-pin.png');
const grayPin = require('./img/gray-pin.png');

export const yearRanges = [
  {
    min: moment().add(1, 'years').year(),
    max: 2999,
    label: 'Incomplete',
    pin: yellowPin,
    color: Color.COLOR_YEAR_FUTURE
  }, 
  {
    min: moment().year(),
    max: moment().year(),
    label: 'This Year',
    pin: greenPin,
    color: Color.COLOR_THIS_YEAR
  }, 
  {
    min: moment().subtract(5, 'years').year(),
    max: moment().subtract(1, 'years').year(),
    label: '1-5 years',
    pin: orangePin,
    color: Color.COLOR_YEAR_0_5
  }, 
  {
    min: moment().subtract(10, 'years').year(),
    max: moment().subtract(6, 'years').year(),
    label: '6-10 years',
    pin: bluePin,
    color: Color.COLOR_YEAR_5_10
  }, 
  {
    min: 1000,
    max: moment().subtract(11, 'years').year(),
    label: '< 10 years',
    pin: grayPin,
    color: Color.COLOR_YEAR_10UP
  }, 
];

const findRangeByYear = (ranges, year) => {
  if (year === 'all') return { min: 1000, max: 2999 };
  const yearNumber = _.toNumber(year);
  return _.find(ranges, range => yearNumber <= range.max && yearNumber >= range.min);
}

export const getPinByYearComplete = (yearComplete) => {
  const range = findRangeByYear(yearRanges, yearComplete);
  return _.get(range, 'pin', grayPin);
};

export const getColorByYearComplete = (yearComplete) => {
  const range = findRangeByYear(yearRanges, yearComplete);
  return _.get(range, 'color', Color.COLOR_YEAR_10UP);
}

export const filterProjectsByRadius = (projects, { lat, lng }, radius = 500, query = {}) => {
    const { budget, yearComplete } = query;
    const budgetOption = _.filter(budget.options, opt => _.includes(budget.value, opt.value));
    
    let yearCompleteOption;
    if (yearComplete.value === 'all') {
      yearCompleteOption =  [{ max: 2999, min: 1000 }];
    } else {
      yearCompleteOption = _.filter(yearRanges, opt => {
        return _.includes(yearComplete.value, `${opt.min}-${opt.max}`);
      });

      // const [ymin, ymax] = _.map(yearComplete.value.split('-'), _.toNumber);
      // yearCompleteOption = _.find(yearRanges, opt => opt.min === ymin && opt.max === ymax);
    }
    // console.log('yearComplete', yearComplete)
    // console.log('yearRanges', yearRanges)
    // console.log('yearCompleteOption', yearCompleteOption)
    
    // console.log('filterProjectsByRadius', query, yearCompleteOption);
    return _.filter(projects, p => {
      const pp = _.get(p, 'location.position', {_lat: 0, long: 0});
      const d = getDistance(pp._lat, pp._long, lat, lng);
      // console.log('d', d);
      
      let result = d <= 0.001 * radius
        && (_.some(budgetOption, bO => {
          return _.inRange(p.current.salePrice, bO.min, bO.max)
        }) || _.size(budgetOption) === 0)
        && (_.some(yearCompleteOption, yO => {
          return _.inRange(_.toNumber(p.yearComplete), yO.min, yO.max + 1)
        }) || _.size(yearCompleteOption) === 0);
        

        // && budgetOption.min <= p.current.salePrice
        // && budgetOption.max >= p.current.salePrice
        // && yearCompleteOption.min <= _.toNumber(p.yearComplete)
        // && yearCompleteOption.max >= _.toNumber(p.yearComplete);
  
      return result;
    });
  }