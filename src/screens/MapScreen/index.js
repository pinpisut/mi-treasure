import React from 'react';
import { Helmet } from 'react-helmet';
import styled from 'styled-components';
import _ from 'lodash';
import { Route, Link, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import IconLaunch from '@material-ui/icons/Launch';
import Button from '@material-ui/core/Button';
import Hidden from "@material-ui/core/Hidden";


import Popover from '@material-ui/core/Popover';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';

import IconVisibility from '@material-ui/icons/Visibility';
import IconVisibilityOff from '@material-ui/icons/VisibilityOff';
import IconFavorite from '@material-ui/icons/Favorite';
import IconFavoriteBorder from '@material-ui/icons/FavoriteBorder';

import IconExpandMore from '@material-ui/icons/ExpandMore';
import IconCheckboxOutline from '@material-ui/icons/CheckBoxOutlineBlank';

import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';

import HeatmapLayer from 'react-google-maps/lib/components/visualization/HeatmapLayer';

import { searchProjects, showProjectOnMap, showAllProjectOnMap, hideProjectOnMap, hideAllProjectOnMap, searchStation } from './actions';
import { yearRanges, getPinByYearComplete } from './utils';

import SearchBar from '../../components/SearchBar';
import ContentTab from '../../components/ContentTab';
import MapLegend from '../../components/MapLegend';
import Map, { Marker, Circle, OverlayView, Polyline } from '../../components/Map';

import WatermarkOverlay from '../../components/WatermarkOverlay';
import Overview from '../../components/SummaryContainer/Overview';
import Projects from '../../components/SummaryContainer/Projects';
import Analysis from '../../components/SummaryContainer/Analysis';
import Facilities from '../../components/SummaryContainer/Facilities';

import * as Colors from '../../components/Color';

import { offsetPosition } from '../../utils/map-utils';

import { addAllToCompare, removeAllFromCompare } from '../../actions/compare-actions';

import { db } from '../../engine';
import { mapStyles } from './styles';
import { ButtonHideTab, Main, SearchBarContainer, ResultContainer, MapContainer, SummaryContainerInner, SummaryContainer } from './components';
import ProjectOverlayView from './ProjectOverlayView';

import Container from './styled.js';

const markerSchool = require('./img/icon-school.png');
const markerHospital= require('./img/icon-hospital.png');
const markerPark = require('./img/icon-park.png');
const markerShopping = require('./img/icon-shopping.png');
const markerOffice = require('./img/icon-office.png');
const markerTransit = require('./img/icon-transit.png');

const FACILITIES = [
  'hospital',
  'school',
  'park',
  'office',
  'shopping',
  'transportation',
];

const DEFAULT_RADIUS = 600;

const getIconUrl = type => {
  switch (type) {
    case 'shopping':
    case 'shopping_mall':
    case 'supermarket':
      return markerShopping;
    case 'park':
      return markerPark;
    case 'school':
    case 'education':
      return markerSchool;
    case 'hospital':
      return markerHospital;
    case 'office':
      return markerOffice
    case 'transportation':
      return markerTransit
    default:
      return markerPark;
  }
}

// const getPixelPositionOffset = (width, height) => ({
//   x: -(width / 2),
//   y: -(height + 40),
// });

const getCircleLabelPixelPositionOffset = (width, height) => ({
  x: -(width / 2),
  y: -(height / 2),
});

const TabIcon = styled.span`
  margin-right: 2px;
  font-size: 1.3em;
  position: relative;
  top: 2px;
`;

const mapStateToProps = (state, ownProps) => {
  const pointOfInterestEntities = _.get(state, 'entities.pointOfInterests.entities');
  const projectEntities = _.get(state, 'entities.projects.entities');
  const stationOnMap = _.get(state, 'domain.map.stationOnMap', []);

  console.log('MapScreen',ownProps)

  // Domain
  const mapReducer = _.get(state, 'domain.map');
  const { query, result } = mapReducer;

  const poiId = ownProps.match.params.poi;
  let pointOfInterest = {};
  if (/^@/.test(poiId)) {
    // Is Lat Lng
    const [lat, lng] = _.split(_.replace(poiId, '@', ''), ',');
    pointOfInterest = {
      location: {
        position: {
          _lat: _.toNumber(lat),
          _long: _.toNumber(lng),
        }
      }
    }
  } else {
    // is poiId
    pointOfInterest = _.get(pointOfInterestEntities, poiId);
  }

  const visibleProjects = _.map(result, pId => _.get(projectEntities, pId));

  
  return {
    ...ownProps,
    overviewPosition: state.domain.design.overviewPosition,
    query,
    poiId,
    pointOfInterest,
    allProjects: projectEntities,
    visibleProjects,
    stationOnMap
  }
}

const mapDispatchToProps = dispatch => {
  return {
    dispatch,
    searchStation: (radius) => dispatch(searchStation(radius)),
    searchProjects: (poiId, radius) => dispatch(searchProjects(poiId, radius)),
    clickAddAllToCompare: (ids) => dispatch(addAllToCompare(ids)),
    clickRemoveAllFromCompare: () => dispatch(removeAllFromCompare()),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(class extends React.PureComponent {

  static getDerivedStateFromProps(nextProps, prevState) {

    if (nextProps.pointOfInterest !== prevState.prevPointOfInterest) {
      const lat = _.get(nextProps.pointOfInterest, 'location.position._lat');
      const lng = _.get(nextProps.pointOfInterest, 'location.position._long');
  
      return {
        mapCenter: { lat, lng },
        prevPointOfInterest: nextProps.pointOfInterest,
        facilities: null,
      }
    }

    if (nextProps.visibleProjects !== prevState.prevVisibleProjects) {
      return {
        // userSelectedProjects: _.map(nextProps.visibleProjects, p => p.id),
        prevVisibleProjects: nextProps.visibleProjects,
      }
    }

    return null;
  }

  state = {
    openViewOption: false,
    zoom: 17,
    visibleList: [
      'yearComplete',
      'salePrice',
      'rentPrice',
      'capitalGain',
      'rentalYield'
    ],
    prevVisibleProjects: [],
    // userSelectedProjects: [],
    facilities: null,
    hideTab: true,
    openMenu: false,
  }
  
  openMenu = (e) => {
    e.preventDefault();
    this.setState({ openMenu: !this.state.openMenu });
    console.log('openMenu');
  }
  handleCloseMenu = (e) => {
    e.preventDefault();
    console.log('handleCloseMenu');
    this.setState({ openMenu: false });
    console.log('handleCloseMenu2');
  }

  componentDidMount = async () => {
    const { searchProjects, searchStation } = this.props;
    this.loadFacilities(this.props.pointOfInterest);

    // await dispatch(initApplication());
    const { query, poiId } = this.props;
    const radius = _.get(query, 'radius', DEFAULT_RADIUS);
    searchProjects(poiId, radius);
    searchStation(radius);
  }

  componentDidUpdate = (prevProps, prevState) => {
    // Load External Data on props change
    if (this.state.facilities === null) {
      this.loadFacilities(this.props.pointOfInterest);
    }

    // Side Effect on props change
    const { pointOfInterest } = this.props;
    if (prevProps.poiId !== this.props.poiId) {
      const lat = _.get(pointOfInterest, 'location.position._lat');
      const lng = _.get(pointOfInterest, 'location.position._long');
      if (this.map) {
        this.map.panTo({ lat, lng });
      }

      this.summaryContainer.scrollTop = 0;
      
      const { query, dispatch } = this.props;
      const radius = _.get(query, 'radius', DEFAULT_RADIUS);
      dispatch(searchProjects(this.props.poiId, radius));
    }
  }

  loadFacilities = (pointOfInterest) => {
    this.setState({
      facilities: [],
    });
    /*
    const { query } = this.props;
    const lat = _.get(pointOfInterest, 'location.position._lat');
    const lng = _.get(pointOfInterest, 'location.position._long');
    const radius = _.get(query, 'radius', DEFAULT_RADIUS);

    fetchNearbyPlaces(lat, lng, radius)
      .then(facilities => {
        this.setState({
          facilities,
        });
      });
      */
  }

  handleFilterChange = (name, value) => {
    const { poiId, query, dispatch } = this.props;
    const radius = _.get(query, 'radius', DEFAULT_RADIUS);
    dispatch({
      type: 'MAP/QUERY/CHANGE',
      name,
      value,
    });

    dispatch(searchProjects(poiId, radius));
  }

  handleClearFilter = () => {
    const { poiId, query, dispatch } = this.props;
    const radius = _.get(query, 'radius', DEFAULT_RADIUS);

    dispatch({
      type: 'MAP/QUERY/CHANGE',
      name: 'yearComplete',
      value: [],
    });

    dispatch({
      type: 'MAP/QUERY/CHANGE',
      name: 'budget',
      value: [],
    });

    dispatch(searchProjects(poiId, radius));
  }

  handleCircleRadiusChange = (e) => {
    const { dispatch, poiId } = this.props;
    const radius = this.boundingCircle.getRadius();

    dispatch({
      type: 'MAP/QUERY/CHANGE/RADIUS',
      radius
    });

    dispatch(searchProjects(poiId, radius));
  }

  handleCircleCenterChange = (e) => {
    const center = this.boundingCircle.getCenter();
    const { history } = this.props;
    history.push(`/map/@${center.lat()},${center.lng()}`);
  }

  handleZoomChange = (e) => {
    this.setState({
      zoom: this.map.getZoom(),
    });
  }

  handleSelectProject = (pId, checked) => {
    const { dispatch } = this.props;
    if (checked) {
      dispatch(showProjectOnMap(pId));
    } else {
      dispatch(hideProjectOnMap(pId));
    }
  }

  handleToggleAllProjects = (value, checked) => {
    const { dispatch, visibleProjects } = this.props;
    if (checked) {
      dispatch(showAllProjectOnMap(_.map(visibleProjects, p => p.id)));
    } else {
      dispatch(hideAllProjectOnMap());
    }
  }

  handelHideTab = () => {
    const tab = this.state.hideTab;
    this.setState({
      hideTab: !tab
    });
  }

  handleClickContentTab = (e) => {
    this.setState({
      hideTab: e
    });
  }

  renderPlaceMarker = (place, type) => {
    return (
      <Marker
        key={place.id}
        position={{
          lat: _.get(place, 'geometry.location.lat'),
          lng: _.get(place, 'geometry.location.lng')
        }}
        options={{
          icon: {
            url: getIconUrl(type),
            size: { width: 20, height: 20 },
            scaledSize: { width: 20, height: 20 },
          }
        }}
      />
    )
  }

  renderLocationRadius = (lat, lng, radius) => {

    const { draggingCircle } = this.state;
    const labelPos = offsetPosition(lat, lng, radius * Math.cos(0.2), -radius * Math.sin(0.2));

    return (
      <React.Fragment>
        <Circle
          center={{ lat, lng }}
          radius={radius}
          ref={c => this.boundingCircle = c}
          onRadiusChanged={this.handleCircleRadiusChange}
          onCenterChanged={this.handleCircleCenterChange}
          options={{
            editable: true,
            fillColor: Colors.primary,
            fillOpacity: draggingCircle ? 0.3 : 0.05,
            strokeColor: Colors.primary,
            strokeOpacity: 0.4,
            strokeWeight: draggingCircle ? 6 : 2,
          }}
        />
        <Polyline
          path={[
            { lat, lng },
            labelPos,
          ]}
          options={{
            strokeColor: Colors.primary,
            strokeOpacity: 1.0,
            strokeWeight: 2,
          }}
        />
        <Circle
          center={{ lat, lng }} radius={10}
          options={{
            fillColor: Colors.primary,
            fillOpacity: 1,
            strokeWeight: 0,
          }}
        />
        <OverlayView
          position={labelPos}
          mapPaneName={OverlayView.OVERLAY_MOUSE_TARGET}
          getPixelPositionOffset={getCircleLabelPixelPositionOffset}
        >
          <div
            style={{
              background: Colors.primary,
              color: 'white',
              width: 40,
              height: 40,
              textAlign: 'center',
              borderRadius: 40,
              lineHeight: '40px',
            }}
          >{radius.toFixed(0)}m</div>
        </OverlayView>
      </React.Fragment>
    )
  }

  renderProjectMarkers = (projects) => {

    return _.map(projects, (project, index) => {
      return (
        <Marker
          key={project.id}
          label={`${index + 1}`}
          position={{
            lat: _.get(project, 'location.position._lat'),
            lng: _.get(project, 'location.position._long')
          }}
          options={{
            icon: {
              url: getPinByYearComplete(project.yearComplete),
              size: { width: 32, height: 40 },
              scaledSize: { width: 32, height: 40 },
              labelOrigin: { x: 17, y: 16 },
            }
          }}
        />
      );
    })
  }

  renderFacilitiesMapElements = () => {
    const { facilities } = this.state;
    return (
      <React.Fragment>
        {_.map(FACILITIES, type =>
          _.map(_.get(facilities, type, []), place => this.renderPlaceMarker(place, type))
        )}
      </React.Fragment>
    );
  }

  renderFacilitiesDetail = () => {
    const { facilities } = this.state;
    console.log('facilitiesfacilities',facilities)
    return _.map(FACILITIES, type =>
      <Card key={type}>
        <CardContent>
          <div style={{ display: 'flex', flexDirection: 'row', textAlign: 'left' }}>
            <div style={{ flexBasis: '30%' }}>
              <img src={getIconUrl(type)} width={22} alt="" /> {_.upperFirst(type)}
            </div>
            <div style={{ flexBasis: '70%' }}>
              {_.map(_.get(facilities, type, []), place =>
                <div key={place.id} style={{ color: '#787878', padding: 0, marginBottom: 16 }}>{place.name}</div>
              )}
            </div>
          </div>
        </CardContent>
      </Card>
    );
  }

  renderAnalysisMapRadius = (lat, lng, radius) => {

    const { draggingCircle } = this.state;
    const labelPos = offsetPosition(lat, lng, radius * Math.cos(0.2), -radius * Math.sin(0.2));

    return (
      <React.Fragment>
        <Circle
          center={{ lat, lng }}
          radius={radius}
          options={{
            fillColor: Colors.primary,
            fillOpacity: draggingCircle ? 0.3 : 0.05,
            strokeColor: Colors.primary,
            strokeOpacity: 0.4,
            strokeWeight: draggingCircle ? 6 : 2,
          }}
        />
      </React.Fragment>
    )
  }

  renderAnalysisMapElements = (projects, radius) => {
    const heatmapData = _.map(projects, project => {
      const lat = _.get(project, 'location.position._lat');
      const lng = _.get(project, 'location.position._long');
      const visibleProject = _.get(project, 'visibleProjects');
      console.log(' _.size(visibleProject)', _.size(visibleProject))
      return (visibleProject && _.size(visibleProject) > 0) ? this.renderAnalysisMapRadius(lat, lng, radius) : null
    });

    return heatmapData;
  }

  render() {
    const { hideTab, facilities } = this.state;
    const { query, match, overviewPosition, pointOfInterest, visibleProjects, stationOnMap } = this.props;
    const lat = _.get(pointOfInterest, 'location.position._lat');
    const lng = _.get(pointOfInterest, 'location.position._long');
    
    const radius = _.get(query, 'radius', DEFAULT_RADIUS);
    const zoom = _.get(this.state, 'zoom', 16);

    const { poi, page } = _.get(match, 'params');

    const projectIds = _.map(visibleProjects, p => p.id);

    const { openMenu } = this.state;
    const { clickAddAllToCompare, clickRemoveAllFromCompare } = this.props;

    return (
      <Container>
        <Main mode={this.state.mode}>
          <SearchBarContainer>
            <div className="box-search">
              <SearchBar
                db={db}
                pointOfInterest={pointOfInterest}
                query={query}
                yearRanges={yearRanges}
                onFilterChange={this.handleFilterChange}
                onToggleViewProjects={this.toggleDrawer}
                poi={poi}
                handleClearFilter={this.handleClearFilter}
              />
            </div>
            <Hidden smDown>
              <div className="box-content-tab">
                <ContentTab poi={poi} page={page} open={!hideTab} onClickContentTab={this.handleClickContentTab}/>
              </div>
            </Hidden>
          </SearchBarContainer>
          <ResultContainer>
            <MapContainer>
              {
                pointOfInterest &&
                  <Map
                    innerRef={c => this.map = c}
                    googleMapURL={`https://maps.googleapis.com/maps/api/js?key=${process.env.REACT_APP_GOOGLE_APIKEY_BROWSER}&v=3.exp&libraries=geometry,drawing,places,visualization`}
                    defaultCenter={{ lat, lng }}
                    defaultZoom={zoom}
                    onZoomChanged={this.handleZoomChange}
                    options={{
                      styles: mapStyles,
                      mapTypeControl: false,
                      fullscreenControl: false,
                      zoomControl: true,
                      streetViewControl: false,
                    }}
                    loadingElement={<div style={{ height: `100%` }} />}
                    containerElement={<div style={{ height: `100%` }} />}
                    mapElement={<div id="map" style={{ height: `100%` }} />}
                  >
                    
                    <Route
                      path={`/map/${poi}/projects`}
                      render={() => (
                        <div>
                          {this.renderLocationRadius(lat, lng, radius)}
                          <ProjectOverlayView zoom={this.state.zoom} projects={visibleProjects} pointOfInterest={pointOfInterest} />
                        </div>
                      )}
                    /> 
                    {/* <Route
                      path={`/map/${poi}/facilities`}
                      render={this.renderFacilitiesMapElements}
                    />*/}
                    <Route
                      path={`/map/${poi}/analysis`}
                      render={() => this.renderAnalysisMapElements(stationOnMap, radius)}
                    /> 
                  </Map>
              }
              <Button
                variant="raised"
                color="primary"
                className="noprint"
                style={{ position: 'absolute', right: 10, top: 10 }}
                onClick={() => window.print()}
              >
                <IconLaunch style={{ fontSize: '1em', marginRight: 5, position: 'relative', top: -2 }} /> Export result
              </Button>
              <Button
                variant="outlined"
                buttonRef={c => this.anchorEl = c}
                onClick={this.openMenu}
                style={{ position: 'absolute', right: 10, top: 55, background: '#fff' }}
              >
                <IconCheckboxOutline />
                <IconExpandMore />
                <Popover
                  anchorEl={this.anchorEl}
                  open={openMenu}
                  anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'left',
                  }}
                  transformOrigin={{
                    vertical: 'top',
                    horizontal: 'left',
                  }}
                >
                  <List>
                    <ListItem button onClick={(e) => this.handleToggleAllProjects(e, true)}>
                      <ListItemIcon><IconVisibility /></ListItemIcon>
                      <ListItemText>View All on Map</ListItemText>
                    </ListItem>
                    <ListItem button onClick={(e) => this.handleToggleAllProjects(e, false)}>
                      <ListItemIcon><IconVisibilityOff /></ListItemIcon>
                      <ListItemText>Hide All from Map</ListItemText>
                    </ListItem>
                    <ListItem button onClick={() => clickAddAllToCompare(projectIds)}>
                      <ListItemIcon><IconFavorite /></ListItemIcon>
                      <ListItemText>Add All To Compare</ListItemText>
                    </ListItem>
                    <ListItem button onClick={clickRemoveAllFromCompare}>
                      <ListItemIcon><IconFavoriteBorder /></ListItemIcon>
                      <ListItemText>Remove All from Compare</ListItemText>
                    </ListItem>
                  </List>
                </Popover>
              </Button>
              <MapLegend yearRanges={yearRanges} />
              {overviewPosition === 'B' && <Overview layout="B" pointOfInterest={pointOfInterest} projects={visibleProjects} />}
            </MapContainer>
            <SummaryContainer hide={hideTab}>
              <ButtonHideTab hide={hideTab} onClick={this.handelHideTab}/>
              {/* <Tabs
                value={page || 'overview'}
                indicatorColor="primary"
                textColor="primary"
                fullWidth
                style={{ background: 'white' }}
              >
                {overviewPosition === 'A' && <Tab component={Link} value="overview" to={`/map/${poi}`} label={<span><TabIcon className="icon-tab-overview" /> Overview</span>} />}

                <Tab component={Link} value="projects" to={`/map/${poi}/projects`} label={<span><TabIcon className="icon-tab-project" /> Projects ({_.size(visibleProjects)})</span>} />

                <Tab disabled component={Link} value="facilities" to={`/map/${poi}/facilities`} label={<span><TabIcon className="icon-tab-facility" /> Facilities</span>} />

                <Tab disabled component={Link} value="analysis" to={`/map/${poi}/analysis`} label={<span><TabIcon className="icon-tab-analysis" /> Analysis</span>} />
              </Tabs> */}
              <SummaryContainerInner innerRef={node => this.summaryContainer = node}>
                {overviewPosition === 'A' ? 
                  <Route
                    exact
                    path={`/map/${poi}`}
                    render={routerProps => <Overview location={poi} projects={visibleProjects} />}
                  />
                  :
                  <Redirect to={`/map/${poi}/projects`} />
                }
                <Route
                  path={`/map/${poi}/projects`}
                  render={routerProps =>
                    <Projects
                      {...routerProps}
                      poi={poi}
                      pointOfInterest={pointOfInterest}
                      projects={visibleProjects}
                      onToggleSelectAll={this.handleToggleAllProjects}
                      onSelectProject={this.handleSelectProject}
                    />
                  }
                />
                <Route
                  path={`/map/${poi}/facilities`}
                  render={routerProps => 
                  <Facilities {...routerProps} pointOfInterest={pointOfInterest} detail={facilities} getIconUrl={getIconUrl}/>}
                  // render={this.renderFacilitiesDetail}
                />
                <Route
                  path={`/map/${poi}/analysis`}
                  render={routerProps => <Analysis projects={visibleProjects} location={poi} />} />
              </SummaryContainerInner>
            </SummaryContainer>
          </ResultContainer>
          <WatermarkOverlay />
        </Main>
      </Container>
    );
  }
});
