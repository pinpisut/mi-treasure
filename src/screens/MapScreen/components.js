import React from 'react';
import styled, { css, injectGlobal } from 'styled-components';
import { connect } from 'react-redux';


import IconHideDown from '@material-ui/icons/KeyboardArrowDown';
import IconHideUp from '@material-ui/icons/KeyboardArrowUp';

import { Tab_Hide } from '../../components/Color';
import { H6 } from '../../components/Typography';

const BREAKPOINT = 960;
const HEADER_HEIGHT = 60 + 63;
const DETAIL_HEIGHT = 210 + 16 + 26;
const TOP_BOTTOM_HEIGHT = HEADER_HEIGHT + DETAIL_HEIGHT;

const PRINT = 'print';

injectGlobal`
  @media (min-width: ${BREAKPOINT}px) {
    body {
      overflow: hidden;
    }
  }
`;

export const Main = connect(state => ({ layout: state.domain.design.layout }))(styled.div`
  transition: all .5s ease-out;
  position: relative;
  padding-right: ${props => (props.mode !== 'temporary' && props.showDrawer === true ? 530 : 0)}px;
  padding-top: 64px; /* Search Bar Height */
  display: flex;
  flex-direction: column;

  @media ${PRINT} {
    padding: 0;
  }
`);

export const SearchBarContainer = styled.div`
  box-shadow: 0 2px 2px rgba(0, 0, 0, 0.24);
  position: absolute;
  top: 0;
  width: 100%;
  z-index: 2;
  box-sizing: border-box;
  padding: 14px;
  display: flex;
  align-items: center;

  background: black;
  transition: all .3s;

  ${props => props.fixedTop && `position: fixed; top: 0;` }
  -webkit-print-color-adjust: exact;
  @media ${PRINT} {
    position: relative;
  }
`;

export const ResultContainer = connect(state => ({ layout: state.domain.design.layout }))(styled.div`
  display: flex;
  ${props => props.layout === 'A' &&
    `flex-direction: column;`
  }
  ${props => props.layout === 'B' && css`
      flex-direction: column;
      height: auto;
      @media (min-width: ${BREAKPOINT}px) {
        flex-direction: row;
        height: calc(100vh - ${HEADER_HEIGHT}px);
      }
    `
  }

  @media ${PRINT} {
    display: block;
    height: auto;
  }
`);

export const MapContainer = connect(state => ({ layout: state.domain.design.layout }))(styled.div`
  position: relative;
  ${props => props.layout === 'A' &&
    `height: calc(100vh - ${TOP_BOTTOM_HEIGHT}px);`
  }
  ${props => props.layout === 'B' && css`
      flex-grow: 1;
      width: 100%;
      height: calc(100vh - ${HEADER_HEIGHT + 85}px);
      background: black;

      @media (min-width: ${BREAKPOINT}px) {
        width: 60%;
        height: calc(100vh - ${HEADER_HEIGHT + 85}px);
      }
    `
  }

  @media ${PRINT} {
    width: 100%;
    height: 500px;
  }
`);

// export const containerElement = <div style={{ height: `calc(100vh - ${TOP_BOTTOM_HEIGHT}px)` }} />;

export const MapContainerElement = connect(state => ({ layout: state.domain.design.layout }))(styled.div`
  ${props => props.layout === 'A' && `
    height: calc(100vh - ${TOP_BOTTOM_HEIGHT}px);
  `}
  ${props => props.layout === 'B' && `
    height: calc(100vh - ${HEADER_HEIGHT + 85}px);
  `}

    @media ${PRINT} {
      width: 100%;
      height: 500px;
    }
`);

export const SummaryContainer = connect(state => ({ layout: state.domain.design.layout }))(styled.div`
  display: flex;
  flex-direction: column;
  position: relative;
  z-index: 1;
  background: #fafafa;
  box-shadow: 0 0 4px 4px rgba(0, 0, 0, 0.2);
  padding: 0px 10px 0px 22px;
  transition: all 0.3s ease-in-out;

  ${props => props.layout === 'A' && css`
    height: ${DETAIL_HEIGHT};
  `}
  ${props => props.layout === 'B' && css`
    flex: 1 0 100%;
    height: auto;

    @media (min-width: ${BREAKPOINT}px) {
      flex: 1 0 390px;
      height: calc(100vh - ${HEADER_HEIGHT}px);
    }
  `}
  ${props => props.hide && css`
    width: 0px;
    flex-grow: 0 !important;
    flex-basis: 0px !important;
    padding-left: 10px;
  `}

  @media ${PRINT} {
      width: 100%;
      height: auto;
    }
`);

export const SummaryContainerInner = connect(state => ({ layout: state.domain.design.layout }))(styled.div`
  overflow: scroll;
  margin-left: 23px;
  padding-right: 23px;

  ${props => props.layout === 'B' && css`
    height: auto;
    @media (min-width: ${BREAKPOINT}px) {
      height: calc(100vh - 123px);
    }
  `}

  @media ${PRINT} {
    width: 100%;
    height: auto;
    overflow: auto;
  }

`);

export const ButtonHideTab = (props) => {
  const Button = styled.button`
    position: absolute;
    left: 0px;
    top: -1px;
    border: none;
    outline: none;
    width: 22px;
    height: 100vh;
    background-color: ${Tab_Hide};
    box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.13);
    border: none;
    transition: transform 0.2s ease-out, box-shadow 0.4s ease-out;
    &:active{
      box-shadow: none;
      opacity: 0.9;
    }
`;


const Text = styled(H6)`
    font-size: 1rem !important;
    color: #fff !important;
    white-space: nowrap; 
    text-overflow: ellipsis;
    display: block;
    margin-bottom: 0 !important;
`;

const Content = styled.div`
    transform: rotate(270deg);
    display: flex;
    flex-flow: row nowrap;
    align-items: center;
`;


return (
  <Button onClick={props.onClick} hide={props.hide}>
    <Content>
      {props.hide ? <IconHideUp style={{ color: '#fff'}}/> : <IconHideDown style={{ color: '#fff'}}/>}
        <Text style={{ margin: '0px 10px' }}>{props.hide ? 'Show Detail' : 'Hide Detail'}</Text>
      {props.hide ? <IconHideUp style={{ color: '#fff'}}/> : <IconHideDown style={{ color: '#fff'}}/>}
    </Content>
  </Button>)
}


// calc(100vh - ${TOP_BOTTOM_HEIGHT}px)
