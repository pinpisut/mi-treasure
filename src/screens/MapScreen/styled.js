import styled from "styled-components";

export default styled.div`
  .box-search {
    width: 70%;
    @media (max-width: 959px) {
      width: 100%;
    }
  }
  .box-content-tab {
    width: 30%;
    @media (max-width: 1113px) {
      width: 40%;
    }
  }
`;
