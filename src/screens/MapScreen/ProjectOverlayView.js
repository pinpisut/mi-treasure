import React from 'react';
import styled from 'styled-components';
import Color from 'color';
import _ from 'lodash';
import { connect } from 'react-redux';
import Tooltip from '@material-ui/core/Tooltip';


import { OverlayView, Marker } from '../../components/Map';
import MarkToolTip from '../../components/MarkToolTip';
import { P } from '../../components/Typography';


import { getColorByYearComplete, getPinByYearComplete } from './utils';

import { showProjectOnMap, hideProjectOnMap } from './actions';
import { addToCompare, removeFromCompare } from '../../actions/compare-actions';

const ARROWSIZE = 6;

const ProjectInfoWindow = styled.div`
  box-sizing: border-box;
  color: black;
  background: ${props =>
    props.activeColor.backgroundColor || 'rgba(0, 0, 0, 0.8)'};
  border: 2px solid
    ${props =>
      Color(props.activeColor.borderColor)
        .darken(0.2)
        .string() || 'black'};
  padding: 0px 8px;
  border-radius: 4px;
  position: relative;
  z-index: ${props => (props.active ? '2' : '1')};
  box-shadow: ${props =>
    props.active ? '0 0 10px rgba(0, 0, 0, 0.25)' : 'unset'};
  text-align: left;
  transition: all 0.4s;

  display: ${props => (props.showDetail ? 'block' : 'none')}

  &:hover {
    z-index: 9999;
    box-shadow: 0 0 10px rgba(0, 0, 0, 0.25);
  }

  &:active {
    z-index: 9999;
    box-shadow: 0 0 10px rgba(0, 0, 0, 0.25);
  }

  &::before {
    content: '';
    width: 0;
    height: 0;
    position: absolute;
    z-index: 1;
    border-left: ${ARROWSIZE}px solid transparent;
    border-right: ${ARROWSIZE}px solid transparent;
    border-top: ${ARROWSIZE}px solid
      ${props =>
        Color(props.activeColor.borderColor)
          .darken(0.2)
          .string() || 'black'};
    position: absolute;
    bottom: -${ARROWSIZE + 1}px;
    left: 50%;
    margin-left: -${ARROWSIZE}px;
  }

  &::after {
    content: '';
    width: 0;
    height: 0;
    position: absolute;
    z-index: 1;
    border-left: ${ARROWSIZE}px solid transparent;
    border-right: ${ARROWSIZE}px solid transparent;
    border-top: ${ARROWSIZE}px solid
      ${props => props.activeColor.borderColor || 'rgba(0, 0, 0, 0.8)'};
    position: absolute;
    bottom: -${ARROWSIZE}px;
    left: 50%;
    margin-left: -${ARROWSIZE}px;
  }
`;

const Circle = styled.div`
  width: 25px;
  height: 25px;
  background-color: #fa4a4d;
  border: 1px solid #fff;
  border-radius: 50%;
  position: absolute;
  top: -15px;
  right: -13px;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const Noti = styled(P)`
  margin: 0px !important;
  color: #fff !important;
`;


const getPixelPositionOffset = (width, height) => ({
  x: -(width / 2),
  y: -(height + 40)
});

const mapStateToProps = (state, ownProps) => {
  // Domain
  const mapReducer = _.get(state, 'domain.map');
  const compareProjectIds = _.get(state, 'domain.compare.ids');
  const { initVisibleOnMapProjects, visibleOnMapProjects, viewOptionList } = mapReducer;
  return {
    viewOptionList,
    projects: ownProps.projects,
    userSelectedProjects: initVisibleOnMapProjects,
    visibleOnMapProjects,
    compareProjectIds
  };
};

export default connect(mapStateToProps)(
  class extends React.PureComponent {
    state = {
      showDetail: false,
      hoverMark: null,
      
    };

    checkActivePoject = (project, idInterest) => {
      const proId = _.last(_.split(project.id, '.'));
      const interId = _.last(_.split(idInterest.id, '.'));
      return _.isEqual(proId, interId);
    };
    getColor = (project, idInterest) => {
      const activeProject = this.checkActivePoject(project, idInterest);
      if (activeProject) {
        return {
          backgroundColor: '#ffff',
          borderColor: getColorByYearComplete(project.yearComplete)
        };
      }
      return {
        backgroundColor: getColorByYearComplete(project.yearComplete),
        borderColor: getColorByYearComplete(project.yearComplete)
      };
    };

    getDisplayShowDetail = (id, value) => {
      const { hoverMark } = this.state;
      const isHover = _.isEqual(hoverMark, id);
      if (isHover) return true;
      if (value) return true;
      return false;
    }

    handleSelectProject = (id) => {
      const { dispatch, visibleOnMapProjects } = this.props;
      const selectedProject = _.includes(visibleOnMapProjects, id);
      if (selectedProject === true) dispatch(hideProjectOnMap(id));
      else dispatch(showProjectOnMap(id));
    };

    handleCompareProject = id => {
      const { dispatch, compareProjectIds } = this.props;
      const projectAdddedToCompare = _.some(
        compareProjectIds,
        proId => proId === id
      );

      if (!projectAdddedToCompare) {
        dispatch(addToCompare(id));
      } else {
        dispatch(removeFromCompare(id));
      }
    };
    handleLinkProject = id => {};

    handlePopoverOpen = id => {
      this.setState({ hoverMark: id });
    };

    handlePopoverClose = id => {
      const currHover = this.state.hoverMark;
      if (!_.isEqual(currHover, id)) {
        this.setState({ hoverMark: null });
      }
    };

    handleActionBar = (type, id, value) => {
      switch (type) {
        case 'theEye':
          this.handleSelectProject(id);
          break;
        case 'compare':
          this.handleCompareProject(id);
          break;
        case 'link':
          this.handleLinkProject(id);
          break;
        default:
          break;
      }
    };

    render() {
      const {
        viewOptionList,
        projects,
        userSelectedProjects,
        compareProjectIds,
        pointOfInterest,
        visibleOnMapProjects
      } = this.props;
      const { hoverMark } = this.state;
      return _.map(projects, (project, id) => {
        // const mark = _.includes(userSelectedProjects, project.id);
        const showDetail = _.includes(visibleOnMapProjects, project.id);
        const isHover = _.isEqual(hoverMark, project.id);

        return (
          <OverlayView
            key={id}
            position={{
              lat: _.get(project, 'location.position._lat'),
              lng: _.get(project, 'location.position._long')
            }}
            mapPaneName={OverlayView.OVERLAY_MOUSE_TARGET}
            getPixelPositionOffset={getPixelPositionOffset}
            onMouseOut={() => this.handlePopoverClose(project.id)}
          >
            <ProjectInfoWindow
              key={id}
              active={this.checkActivePoject(project, pointOfInterest)}
              activeColor={this.getColor(project, pointOfInterest)}
              showDetail={this.getDisplayShowDetail(project.id, showDetail)}
            >
              { !_.isEqual(project.stock, 0) &&
                <Tooltip placement='top' title={`${project.stock} ${project.stock === 1 ? 'Unit' : 'Units'} Available On The Agent Website`}>
                  <Circle><Noti>{project.stock}</Noti></Circle>
                </Tooltip>
              }
              <Marker
                key={`${project.id}-marker`}
                label={`${id + 1}`}
                position={{
                  lat: _.get(project, 'location.position._lat'),
                  lng: _.get(project, 'location.position._long')
                }}
                opacity={isHover ? 1 : 0.5}
                onClick={() => this.handleSelectProject(project.id)}
                onMouseOver={() => this.handlePopoverOpen(project.id)}
                onMouseOut={() => this.handlePopoverClose(project.id)}
                ref={c => {
                  this.anchorEl = c;
                }}
                options={{
                  icon: {
                    url: getPinByYearComplete(project.yearComplete),
                    size: { width: 32, height: 40 },
                    scaledSize: { width: 32, height: 40 },
                    labelOrigin: { x: 17, y: 16 }
                  }
                }}
              />
              <MarkToolTip
                isHover={isHover}
                key={`${project.id}-tooltip`}
                checked={showDetail}
                project={project}
                compareProjectIds={compareProjectIds}
                viewOptionList={viewOptionList}
                showDetail={showDetail}
                onValueProject={this.handleActionBar}
              />
            </ProjectInfoWindow>
          </OverlayView>
        );
      });
    }
  }
);
