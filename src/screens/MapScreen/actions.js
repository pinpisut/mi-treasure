import _ from 'lodash';
import { filterProjectsByRadius } from './utils';

import { fetchPointOfInterest, fetchPointOfInterests } from '../../api';

// export const initApplication = () => {
//   return (dispatch) => {
//     return fetchPointOfInterests()
//       .then(pointOfInterests => {
//         dispatch({
//           type: 'ENTITIES/POINT_OF_INTERESTS/RECEIVED',
//           items: pointOfInterests,
//         });

//         const projects = _.filter(pointOfInterests, poi => poi.type === 'PROJECT');
//         dispatch({
//           type: 'ENTITIES/PROJECTS/RECEIVED',
//           items: projects,
//         });
//       });
//   }
// }

export const searchProjects = (poiId, radius) => {
  return (dispatch, getState) => {
    const state = getState();
    const projectEntities = _.get(state, 'entities.projects.entities');
    
    const pointOfInterest = _.get(state, ['entities', 'pointOfInterests', 'entities', poiId]);
    // Domain
    const mapReducer = _.get(state, 'domain.map');
    const { query } = mapReducer;
    
    // if poinId is /@lat,lng
    let lat = 0;
    let lng = 0;
    if (/^@/.test(poiId)) {
      // Is Lat Lng
      [lat, lng] = _.split(_.replace(poiId, '@', ''), ',');
    } else {
      lat = _.get(pointOfInterest, 'location.position._lat');
      lng = _.get(pointOfInterest, 'location.position._long');
    }
    // จริงๆ ควรจะยิง API
    const visibleProjects = filterProjectsByRadius(projectEntities, { lat, lng }, radius, query);
  
    dispatch({
      type: 'MAP/RESULT/UPDATE',
      result: visibleProjects,
    })
  }
}

export const getVisibleProjects = (poiId, radius, state) => {
  const projectEntities = _.get(state, 'entities.projects.entities');
  
  const pointOfInterest = _.get(state, ['entities', 'pointOfInterests', 'entities', poiId]);
  // Domain
  const mapReducer = _.get(state, 'domain.map');
  const { query } = mapReducer;
  
  // if poinId is /@lat,lng
  let lat = 0;
  let lng = 0;
  if (/^@/.test(poiId)) {
    // Is Lat Lng
    [lat, lng] = _.split(_.replace(poiId, '@', ''), ',');
  } else {
    lat = _.get(pointOfInterest, 'location.position._lat');
    lng = _.get(pointOfInterest, 'location.position._long');
  }
  // จริงๆ ควรจะยิง API
  const visibleProjects = filterProjectsByRadius(projectEntities, { lat, lng }, radius, query);
  return visibleProjects;
}

export const searchStation = (radius) => {
  return (dispatch, getState) => {
    const state = getState();
    const pointOfInterests = _.get(state, 'entities.pointOfInterests.entities');

    // Result

    const getStations = _.filter(pointOfInterests, o => {
      return o.type === 'BTS' || o.type === 'MRT'
    });

    const stations = _.map(getStations, (value) => {
      const visibleProjects = getVisibleProjects(value.id, radius, state);
      return {...value, visibleProjects}
    });
  
    dispatch({
      type: 'MAP/RESULT/STATIONS',
      result: stations,
    })
  }
}

export const showProjectOnMap = (id) => ({ type: 'DOMAIN/MAP/PROJECT/SHOWON/MAP', id });
export const hideProjectOnMap = (id) => ({ type: 'DOMAIN/MAP/PROJECT/HIDEON/MAP', id });

export const showAllProjectOnMap = (ids) => ({ type: 'DOMAIN/MAP/PROJECT/SHOWON/MAP/ALL', ids });
export const hideAllProjectOnMap = (id) => ({ type: 'DOMAIN/MAP/PROJECT/HIDEON/MAP/ALL', id });