import styled from 'styled-components';

export const BackgroundContainer = styled.div`
  width: 100vw;
  height: 100vh;
  background: #303030;
`;

export const Wrapper = styled.div`
  width: 100%;
  max-width: 1024px;
  height: 100vh;
  margin: 0 auto;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const FormContainer = styled.div`
  flex: 0 0 448px;
`;

export const Form = styled.form`
  padding: 24px 26px;
  border-radius: 9px;
`;

export const FormHeader = styled.h1`
  margin-top: 0;
`;

export const VersionLabel = styled.div`
  margin: 15px 0;
  font-size: 14px;
  color: #3e3e3e;
  position: absolute;
  left: 50px;
  bottom: 50px;
`;

export const ArtworkContainer = styled.div`
  flex: 1 0 50%;
`;

export const LoginGraphicImg = styled.img`
  width: 100%;
`;

export const ErrorMessage = styled.div`
  color: #D71921;
  font-size: 0.875rem;
  padding: 8px 0;
`;